/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly;

import javax.swing.JLabel;
import static monopoly.ChosePlayer.smoney;
import static monopoly.Economy.ShowPlayerInfo;
import static monopoly.Interface.*;
import static monopoly.Move.kill;
import static monopoly.Move.pion;

/**
 *
 * @author Leonescu
 */
public class Player 
{
    
    public static JLabel[] labels= new JLabel[8];
    public static JLabel[] glow= new JLabel[8];
    public static int[] id=new int[8];
    public static int[] sectors=new int[8];
    public static int[] money=new int[8];
    public static int[] lifes=new int[8];
    public static boolean[] dead=new boolean[8];

    public static void init()
    {

        labels= new JLabel[]{Pion1,Pion2,Pion3,Pion4,Pion5,Pion6,Pion7,Pion8};
        glow= new JLabel[]{RoundGlow1,RoundGlow2,RoundGlow3,RoundGlow4,RoundGlow5,RoundGlow6,RoundGlow7,RoundGlow8};
        for(int i=0;i<8;i++){
            lifes[i]=3;
            sectors[i]=0;
            money[i]=smoney;
            dead[i]=true;
            id[i]=i+1;
        }
        
    }
    public static void dead()
    {
        labels[pion].setVisible(false);
        Player.dead[pion] = true;
        kill++;
        Player.money[pion] = 0;
        Player.lifes[pion]=0;
        ShowPlayerInfo(id[pion]);
        Ok.setVisible(false);
        jLabel_ok.setVisible(false);
        for (int i=0;i<36;i++)
            if (Location.owner[i]==pion+1)
            {
            Location.stationsl[i].setBackground(new java.awt.Color(151, 151, 151));
            Location.owner[i]=0;
            Location.stations[i]=0;
            Location.rent[i]=0;
            Location.sellable[i]=true;
            Location.stationsl[i].setText(" ");
            }
    }
}