/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly;

import java.util.ArrayList;
import java.util.Random;
import static monopoly.Interface.info;

/**
 *
 * @author Leonescu
 */
public class Chance {
    
    public static int value2pay;
    public static String dtext;
    public static ArrayList<String> text;
    
    public static void reset2pay()
    {
        System.out.print(value2pay+" before reset\n");
        value2pay=0;
        System.out.print(value2pay+" after reset\n");
    }
    
    public static void init() 
    {
        value2pay=0;
        dtext="";
        text = new ArrayList<>();
        text.add("You died!");
        text.add("You found a new alien race. Get 5000$.");
        text.add("You found a mineable asteroid. Get 1000$.");
        text.add("You found a wreckage. Get 500$.");
        text.add("Someone sent you money. Get 200$.");
        text.add("You won at the lotery. Get 100$.");
        text.add("You won a bet. Get 50$.");
        text.add("You lost at the lotery. Pay 20$.");
        text.add("You lost a bet. Pay 50$.");
        text.add("Your ship needs repairs. Pay 200$.");
        text.add("You have to pay for passage. Pay 300$.");
        text.add("You lost at poker. Pay 500$.");
        text.add("Your ship was stolen.");
        text.add("Your ship needs crew. Pay 1000$.");
        text.add("You crashed your ship.");
        text.add("You crashed into a station. Pay 5000$.");
    }
    public void Draw()
    {
        
        Random r = new Random();
        dtext = text.get(r.nextInt(text.size())-0);
        info.setText("<html><center>Chance:</center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + dtext + "</p></center>" +
                    "</html>");
    }
    public void effect()
    {

        switch (dtext) {
            case "You found a new alien race. Get 5000$.":
                value2pay=-5000;
                break;
            case "You found a mineable asteroid. Get 1000$.":
                value2pay=-1000;
                break;
            case "You found a wreckage. Get 500$.":
                value2pay=-500;
                break;
            case "Someone sent you money. Get 200$.":
                value2pay=-200;
                break;
            case "You won at the lotery. Get 100$.":
                value2pay=-100;
                break;
            case "You won a bet. Get 50$.":
                value2pay=-50;
                break;
            case "You lost at the lotery. Pay 20$.":
                value2pay=20;
                break;
            case "You lost a bet. Pay 50$.":
                value2pay=50;
                break;
            case "Your ship needs repairs. Pay 200$.":
                value2pay=200;
                break;
            case "You have to pay for passage. Pay 300$.":
                value2pay=300;
                break;
            case "You lost at poker. Pay 500$.":
                value2pay=500;
                break;
            case "Your ship was stolen.":
                Player.lifes[Move.pion] -= 1;
                break;
            case "Your ship needs crew. Pay 1000$.":
                value2pay=1000;
                break;
            case "You crashed your ship.":
                Player.lifes[Move.pion] -= 1;
                break;
            case "You crashed into a station. Pay 5000$.":
                Player.lifes[Move.pion] -= 1;
                value2pay=5000;
                break;
            case "You died!":
                Player.dead[Move.pion] = true;
                break;
            default:
                break;
        }
        dtext = "";
    }
    
}
