package monopoly;

import java.awt.Cursor;
import java.awt.Dialog.ModalityType;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import static monopoly.Chance.value2pay;
import static monopoly.ChosePlayer.dev;
import static monopoly.ChosePlayer.n;
import static monopoly.Economy.Build;
import static monopoly.Economy.Buy;
import static monopoly.Economy.Pay;
import static monopoly.Economy.Rent;
import static monopoly.Economy.ShowPlayerInfo;
import static monopoly.Move.DisplaySell;
import static monopoly.Move.dice;
import static monopoly.Move.location;
import static monopoly.Move.pion;
import static monopoly.Player.labels;

public class Interface extends javax.swing.JFrame {

    boolean s;
    
    public static int p;
    Move mov = new Move();
    Economy econ = new Economy();
    Chance change = new Chance();
    
    public void Cursor()
    {
        Toolkit tool = Toolkit.getDefaultToolkit();
        Image curs = tool.getImage("bin/images/cursor.png");
        Point point = new Point(0,0);
        Cursor cursor = tool.createCustomCursor(curs,point,"Cursor");
        setCursor(cursor);
    }
    
    public Interface() {
        
        initComponents();
        initialization();
        Cursor();
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        Pion1 = new javax.swing.JLabel();
        Pion2 = new javax.swing.JLabel();
        Pion3 = new javax.swing.JLabel();
        Pion4 = new javax.swing.JLabel();
        Pion5 = new javax.swing.JLabel();
        Pion6 = new javax.swing.JLabel();
        Pion7 = new javax.swing.JLabel();
        Pion8 = new javax.swing.JLabel();
        DevB1 = new javax.swing.JButton();
        DevB2 = new javax.swing.JButton();
        DevB3 = new javax.swing.JButton();
        Game_Action = new javax.swing.JLayeredPane();
        RoundDisplay = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        RoundGlow1 = new javax.swing.JLabel();
        RoundGlow2 = new javax.swing.JLabel();
        RoundGlow3 = new javax.swing.JLabel();
        RoundGlow4 = new javax.swing.JLabel();
        RoundGlow5 = new javax.swing.JLabel();
        RoundGlow6 = new javax.swing.JLabel();
        RoundGlow7 = new javax.swing.JLabel();
        RoundGlow8 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jRent = new javax.swing.JButton();
        Sell = new javax.swing.JButton();
        Buys = new javax.swing.JButton();
        Buy = new javax.swing.JButton();
        Ok = new javax.swing.JButton();
        Build = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jLabel_ok = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        End = new javax.swing.JButton();
        Rol_label = new javax.swing.JLabel();
        Roll = new javax.swing.JButton();
        jLabel_roll_on = new javax.swing.JLabel();
        jLabel_end_on = new javax.swing.JLabel();
        jLabel_roll_off = new javax.swing.JLabel();
        jLabel_end_off = new javax.swing.JLabel();
        Name8 = new javax.swing.JLabel();
        Name2 = new javax.swing.JLabel();
        zar1 = new javax.swing.JLabel();
        Money3 = new javax.swing.JLabel();
        Money8 = new javax.swing.JLabel();
        sd26 = new javax.swing.JLabel();
        loc26 = new javax.swing.JLabel();
        sd2 = new javax.swing.JLabel();
        sd6 = new javax.swing.JLabel();
        sd17 = new javax.swing.JLabel();
        sd10 = new javax.swing.JLabel();
        sd13 = new javax.swing.JLabel();
        sd7 = new javax.swing.JLabel();
        sd31 = new javax.swing.JLabel();
        sd19 = new javax.swing.JLabel();
        sd1 = new javax.swing.JLabel();
        sd22 = new javax.swing.JLabel();
        sd25 = new javax.swing.JLabel();
        sd24 = new javax.swing.JLabel();
        sd28 = new javax.swing.JLabel();
        sd35 = new javax.swing.JLabel();
        sd8 = new javax.swing.JLabel();
        sd4 = new javax.swing.JLabel();
        sd15 = new javax.swing.JLabel();
        sd11 = new javax.swing.JLabel();
        sd20 = new javax.swing.JLabel();
        sd29 = new javax.swing.JLabel();
        sd33 = new javax.swing.JLabel();
        loc27 = new javax.swing.JLabel();
        loc1 = new javax.swing.JLabel();
        loc7 = new javax.swing.JLabel();
        loc2 = new javax.swing.JLabel();
        loc3 = new javax.swing.JLabel();
        loc5 = new javax.swing.JLabel();
        loc4 = new javax.swing.JLabel();
        loc6 = new javax.swing.JLabel();
        loc10 = new javax.swing.JLabel();
        loc11 = new javax.swing.JLabel();
        loc13 = new javax.swing.JLabel();
        loc9 = new javax.swing.JLabel();
        loc14 = new javax.swing.JLabel();
        loc19 = new javax.swing.JLabel();
        loc8 = new javax.swing.JLabel();
        loc21 = new javax.swing.JLabel();
        loc22 = new javax.swing.JLabel();
        loc23 = new javax.swing.JLabel();
        loc25 = new javax.swing.JLabel();
        loc24 = new javax.swing.JLabel();
        loc18 = new javax.swing.JLabel();
        loc17 = new javax.swing.JLabel();
        loc29 = new javax.swing.JLabel();
        loc31 = new javax.swing.JLabel();
        loc28 = new javax.swing.JLabel();
        loc32 = new javax.swing.JLabel();
        loc30 = new javax.swing.JLabel();
        loc33 = new javax.swing.JLabel();
        loc34 = new javax.swing.JLabel();
        sd5 = new javax.swing.JLabel();
        sd14 = new javax.swing.JLabel();
        sd21 = new javax.swing.JLabel();
        sd36 = new javax.swing.JLabel();
        sd32 = new javax.swing.JLabel();
        loc36 = new javax.swing.JLabel();
        loc12 = new javax.swing.JLabel();
        Name5 = new javax.swing.JLabel();
        Name6 = new javax.swing.JLabel();
        Name7 = new javax.swing.JLabel();
        Money5 = new javax.swing.JLabel();
        Money6 = new javax.swing.JLabel();
        Money4 = new javax.swing.JLabel();
        Money7 = new javax.swing.JLabel();
        Name4 = new javax.swing.JLabel();
        Name3 = new javax.swing.JLabel();
        loc20 = new javax.swing.JLabel();
        loc16 = new javax.swing.JLabel();
        loc15 = new javax.swing.JLabel();
        loc35 = new javax.swing.JLabel();
        sd3 = new javax.swing.JLabel();
        sd16 = new javax.swing.JLabel();
        sd12 = new javax.swing.JLabel();
        sd23 = new javax.swing.JLabel();
        sd30 = new javax.swing.JLabel();
        sd9 = new javax.swing.JLabel();
        sd18 = new javax.swing.JLabel();
        sd27 = new javax.swing.JLabel();
        sd34 = new javax.swing.JLabel();
        zar2 = new javax.swing.JLabel();
        info = new javax.swing.JLabel();
        infosell = new javax.swing.JLabel();
        quit = new javax.swing.JButton();
        jLabel_reset1 = new javax.swing.JLabel();
        jLabel_reset2 = new javax.swing.JLabel();
        Reset = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel_reset = new javax.swing.JLabel();
        menu = new javax.swing.JButton();
        Name1 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        Money2 = new javax.swing.JLabel();
        Money1 = new javax.swing.JLabel();
        Player_list = new javax.swing.JLayeredPane();
        rachetuta = new javax.swing.JLayeredPane();
        rac_p2_3 = new javax.swing.JLabel();
        rac_p2_2 = new javax.swing.JLabel();
        rac_p2_1 = new javax.swing.JLabel();
        rac_p3_3 = new javax.swing.JLabel();
        rac_p3_2 = new javax.swing.JLabel();
        rac_p3_1 = new javax.swing.JLabel();
        rac_p4_3 = new javax.swing.JLabel();
        rac_p4_2 = new javax.swing.JLabel();
        rac_p4_1 = new javax.swing.JLabel();
        rac_p5_3 = new javax.swing.JLabel();
        rac_p5_2 = new javax.swing.JLabel();
        rac_p5_1 = new javax.swing.JLabel();
        rac_p6_3 = new javax.swing.JLabel();
        rac_p6_2 = new javax.swing.JLabel();
        rac_p6_1 = new javax.swing.JLabel();
        rac_p7_3 = new javax.swing.JLabel();
        rac_p7_2 = new javax.swing.JLabel();
        rac_p7_1 = new javax.swing.JLabel();
        rac_p8_3 = new javax.swing.JLabel();
        rac_p8_2 = new javax.swing.JLabel();
        rac_p8_1 = new javax.swing.JLabel();
        rac_p1_3 = new javax.swing.JLabel();
        rac_p1_2 = new javax.swing.JLabel();
        rac_p1_1 = new javax.swing.JLabel();
        PlayerGlow1 = new javax.swing.JLabel();
        PlayerGlow2 = new javax.swing.JLabel();
        PlayerGlow3 = new javax.swing.JLabel();
        PlayerGlow4 = new javax.swing.JLabel();
        PlayerGlow5 = new javax.swing.JLabel();
        PlayerGlow6 = new javax.swing.JLabel();
        PlayerGlow7 = new javax.swing.JLabel();
        PlayerGlow8 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        metal_label = new javax.swing.JLabel();
        background_label = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Monopoly Space Adventure");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setToolTipText("");
        jPanel1.setLayout(null);

        jLayeredPane2.setBackground(new java.awt.Color(255, 204, 0));

        Pion1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion1.png"))); // NOI18N
        jLayeredPane1.add(Pion1);
        Pion1.setBounds(20, 20, 20, 20);

        Pion2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion2.png"))); // NOI18N
        jLayeredPane1.add(Pion2);
        Pion2.setBounds(30, 20, 20, 20);

        Pion3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion3.png"))); // NOI18N
        jLayeredPane1.add(Pion3);
        Pion3.setBounds(30, 30, 20, 20);

        Pion4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion4.png"))); // NOI18N
        jLayeredPane1.add(Pion4);
        Pion4.setBounds(30, 40, 20, 20);

        Pion5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion5.png"))); // NOI18N
        jLayeredPane1.add(Pion5);
        Pion5.setBounds(20, 40, 20, 20);

        Pion6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion6.png"))); // NOI18N
        jLayeredPane1.add(Pion6);
        Pion6.setBounds(10, 40, 20, 20);

        Pion7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion7.png"))); // NOI18N
        jLayeredPane1.add(Pion7);
        Pion7.setBounds(10, 30, 20, 20);

        Pion8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion8.png"))); // NOI18N
        jLayeredPane1.add(Pion8);
        Pion8.setBounds(10, 20, 20, 20);

        DevB1.setText("-1000$");
        DevB1.setBorderPainted(false);
        DevB1.setFocusPainted(false);
        DevB1.setFocusable(false);
        DevB1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DevB1ActionPerformed(evt);
            }
        });
        jLayeredPane1.add(DevB1);
        DevB1.setBounds(100, 250, 100, 50);

        DevB2.setText("-1 life");
        DevB2.setBorderPainted(false);
        DevB2.setFocusPainted(false);
        DevB2.setFocusable(false);
        DevB2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DevB2ActionPerformed(evt);
            }
        });
        jLayeredPane1.add(DevB2);
        DevB2.setBounds(100, 310, 100, 50);

        DevB3.setText(" double 6");
        DevB3.setBorderPainted(false);
        DevB3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DevB3ActionPerformed(evt);
            }
        });
        jLayeredPane1.add(DevB3);
        DevB3.setBounds(100, 370, 100, 50);

        jLayeredPane2.add(jLayeredPane1);
        jLayeredPane1.setBounds(10, 20, 540, 540);

        RoundDisplay.setBackground(new java.awt.Color(51, 51, 51));
        RoundDisplay.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        RoundDisplay.setForeground(new java.awt.Color(159, 181, 193));
        RoundDisplay.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        RoundDisplay.setText("Player1");
        Game_Action.add(RoundDisplay);
        RoundDisplay.setBounds(10, 20, 120, 50);

        jLabel1.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(159, 181, 193));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Turn");
        Game_Action.add(jLabel1);
        jLabel1.setBounds(170, 20, 100, 50);

        RoundGlow1.setBackground(new java.awt.Color(51, 51, 51));
        RoundGlow1.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        RoundGlow1.setForeground(new java.awt.Color(255, 0, 1));
        RoundGlow1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        RoundGlow1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion1.png"))); // NOI18N
        Game_Action.add(RoundGlow1);
        RoundGlow1.setBounds(20, 20, 260, 50);

        RoundGlow2.setBackground(new java.awt.Color(51, 51, 51));
        RoundGlow2.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        RoundGlow2.setForeground(new java.awt.Color(255, 0, 1));
        RoundGlow2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        RoundGlow2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion2.png"))); // NOI18N
        Game_Action.add(RoundGlow2);
        RoundGlow2.setBounds(120, 20, 60, 50);

        RoundGlow3.setBackground(new java.awt.Color(51, 51, 51));
        RoundGlow3.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        RoundGlow3.setForeground(new java.awt.Color(255, 0, 1));
        RoundGlow3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        RoundGlow3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion3.png"))); // NOI18N
        Game_Action.add(RoundGlow3);
        RoundGlow3.setBounds(120, 20, 60, 50);

        RoundGlow4.setBackground(new java.awt.Color(51, 51, 51));
        RoundGlow4.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        RoundGlow4.setForeground(new java.awt.Color(255, 0, 1));
        RoundGlow4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        RoundGlow4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion4.png"))); // NOI18N
        Game_Action.add(RoundGlow4);
        RoundGlow4.setBounds(120, 20, 60, 50);

        RoundGlow5.setBackground(new java.awt.Color(51, 51, 51));
        RoundGlow5.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        RoundGlow5.setForeground(new java.awt.Color(255, 0, 1));
        RoundGlow5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        RoundGlow5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion5.png"))); // NOI18N
        Game_Action.add(RoundGlow5);
        RoundGlow5.setBounds(120, 20, 60, 50);

        RoundGlow6.setBackground(new java.awt.Color(51, 51, 51));
        RoundGlow6.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        RoundGlow6.setForeground(new java.awt.Color(255, 0, 1));
        RoundGlow6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        RoundGlow6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion6.png"))); // NOI18N
        Game_Action.add(RoundGlow6);
        RoundGlow6.setBounds(120, 20, 60, 50);

        RoundGlow7.setBackground(new java.awt.Color(51, 51, 51));
        RoundGlow7.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        RoundGlow7.setForeground(new java.awt.Color(255, 0, 1));
        RoundGlow7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        RoundGlow7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion7.png"))); // NOI18N
        Game_Action.add(RoundGlow7);
        RoundGlow7.setBounds(120, 20, 60, 50);

        RoundGlow8.setBackground(new java.awt.Color(51, 51, 51));
        RoundGlow8.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        RoundGlow8.setForeground(new java.awt.Color(255, 0, 1));
        RoundGlow8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        RoundGlow8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion8.png"))); // NOI18N
        Game_Action.add(RoundGlow8);
        RoundGlow8.setBounds(120, 20, 60, 50);

        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium.png"))); // NOI18N
        jLabel19.setText("jLabel19");
        Game_Action.add(jLabel19);
        jLabel19.setBounds(10, 20, 270, 50);

        jRent.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jRent.setForeground(new java.awt.Color(159, 181, 193));
        jRent.setText("Ok");
        jRent.setBorderPainted(false);
        jRent.setContentAreaFilled(false);
        jRent.setFocusPainted(false);
        jRent.setFocusable(false);
        jRent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRentActionPerformed(evt);
            }
        });
        Game_Action.add(jRent);
        jRent.setBounds(80, 290, 120, 50);

        Sell.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Sell.setForeground(new java.awt.Color(159, 181, 193));
        Sell.setText("Sell");
        Sell.setBorderPainted(false);
        Sell.setContentAreaFilled(false);
        Sell.setFocusPainted(false);
        Sell.setFocusable(false);
        Sell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SellActionPerformed(evt);
            }
        });
        Game_Action.add(Sell);
        Sell.setBounds(80, 290, 120, 50);

        Buys.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Buys.setForeground(new java.awt.Color(159, 181, 193));
        Buys.setText("Buy");
        Buys.setBorderPainted(false);
        Buys.setContentAreaFilled(false);
        Buys.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuysActionPerformed(evt);
            }
        });
        Game_Action.add(Buys);
        Buys.setBounds(80, 290, 120, 50);

        Buy.setBackground(new java.awt.Color(51, 51, 51));
        Buy.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Buy.setForeground(new java.awt.Color(159, 181, 193));
        Buy.setText("Buy");
        Buy.setBorderPainted(false);
        Buy.setContentAreaFilled(false);
        Buy.setFocusPainted(false);
        Buy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuyActionPerformed(evt);
            }
        });
        Game_Action.add(Buy);
        Buy.setBounds(80, 290, 120, 50);

        Ok.setBackground(new java.awt.Color(51, 51, 51));
        Ok.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Ok.setForeground(new java.awt.Color(159, 181, 193));
        Ok.setText("Ok");
        Ok.setBorderPainted(false);
        Ok.setContentAreaFilled(false);
        Ok.setFocusPainted(false);
        Ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OkActionPerformed(evt);
            }
        });
        Game_Action.add(Ok);
        Ok.setBounds(80, 290, 120, 50);

        Build.setBackground(new java.awt.Color(51, 51, 51));
        Build.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Build.setForeground(new java.awt.Color(159, 181, 193));
        Build.setText("Build");
        Build.setBorderPainted(false);
        Build.setContentAreaFilled(false);
        Build.setFocusPainted(false);
        Build.setFocusable(false);
        Build.setRequestFocusEnabled(false);
        Build.setRolloverEnabled(true);
        Build.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuildActionPerformed(evt);
            }
        });
        Game_Action.add(Build);
        Build.setBounds(80, 290, 120, 50);

        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium_background.png"))); // NOI18N
        Game_Action.add(jLabel21);
        jLabel21.setBounds(10, 20, 270, 50);

        jLabel_ok.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_small.png"))); // NOI18N
        jLabel_ok.setText("jLabel2");
        Game_Action.add(jLabel_ok);
        jLabel_ok.setBounds(80, 280, 130, 70);

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_large.png"))); // NOI18N
        Game_Action.add(jLabel18);
        jLabel18.setBounds(10, 90, 270, 330);

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_large_background.png"))); // NOI18N
        jLabel20.setToolTipText("");
        Game_Action.add(jLabel20);
        jLabel20.setBounds(10, 90, 270, 330);

        End.setBackground(new java.awt.Color(51, 51, 51));
        End.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        End.setForeground(new java.awt.Color(153, 153, 153));
        End.setText("End Turn");
        End.setBorderPainted(false);
        End.setContentAreaFilled(false);
        End.setEnabled(false);
        End.setFocusPainted(false);
        End.setFocusable(false);
        End.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EndActionPerformed(evt);
            }
        });
        Game_Action.add(End);
        End.setBounds(20, 510, 250, 50);

        Rol_label.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Rol_label.setForeground(new java.awt.Color(255, 255, 255));
        Rol_label.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Rol_label.setText("Dice:");
        Game_Action.add(Rol_label);
        Rol_label.setBounds(10, 360, 120, 50);

        Roll.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Roll.setForeground(new java.awt.Color(153, 153, 153));
        Roll.setText("Roll Dice");
        Roll.setBorderPainted(false);
        Roll.setContentAreaFilled(false);
        Roll.setFocusPainted(false);
        Roll.setFocusable(false);
        Roll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RollActionPerformed(evt);
            }
        });
        Game_Action.add(Roll);
        Roll.setBounds(20, 450, 250, 50);
        Roll.getAccessibleContext().setAccessibleDescription("");

        jLabel_roll_on.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/buton_on.png"))); // NOI18N
        Game_Action.add(jLabel_roll_on);
        jLabel_roll_on.setBounds(20, 450, 250, 50);

        jLabel_end_on.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/buton_on.png"))); // NOI18N
        Game_Action.add(jLabel_end_on);
        jLabel_end_on.setBounds(20, 510, 250, 50);

        jLabel_roll_off.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/buton_off.png"))); // NOI18N
        Game_Action.add(jLabel_roll_off);
        jLabel_roll_off.setBounds(20, 450, 250, 50);

        jLabel_end_off.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/buton_off.png"))); // NOI18N
        Game_Action.add(jLabel_end_off);
        jLabel_end_off.setBounds(20, 510, 250, 50);

        jLayeredPane2.add(Game_Action);
        Game_Action.setBounds(560, 0, 290, 580);

        Name8.setBackground(new java.awt.Color(51, 51, 51));
        Name8.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Name8.setForeground(new java.awt.Color(159, 181, 193));
        Name8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Name8.setText("Player8");
        jLayeredPane2.add(Name8);
        Name8.setBounds(850, 510, 120, 49);

        Name2.setBackground(new java.awt.Color(51, 51, 51));
        Name2.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Name2.setForeground(new java.awt.Color(159, 181, 193));
        Name2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Name2.setText("Player2");
        jLayeredPane2.add(Name2);
        Name2.setBounds(850, 90, 120, 49);

        zar1.setBackground(new java.awt.Color(51, 51, 51));
        zar1.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        zar1.setForeground(new java.awt.Color(255, 255, 255));
        zar1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        zar1.setText("-");
        jLayeredPane2.add(zar1);
        zar1.setBounds(730, 360, 50, 50);

        Money3.setBackground(new java.awt.Color(51, 51, 51));
        Money3.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Money3.setForeground(new java.awt.Color(159, 181, 193));
        Money3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Money3.setText("-");
        jLayeredPane2.add(Money3);
        Money3.setBounds(1000, 160, 120, 49);

        Money8.setBackground(new java.awt.Color(51, 51, 51));
        Money8.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Money8.setForeground(new java.awt.Color(159, 181, 193));
        Money8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Money8.setText("-");
        jLayeredPane2.add(Money8);
        Money8.setBounds(1000, 510, 120, 49);

        sd26.setBackground(new java.awt.Color(51, 51, 51));
        sd26.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd26.setForeground(new java.awt.Color(0, 0, 0));
        sd26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd26.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd26.setOpaque(true);
        sd26.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd26MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd26);
        sd26.setBounds(80, 490, 50, 20);

        loc26.setBackground(new java.awt.Color(51, 51, 51));
        loc26.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc26.setForeground(new java.awt.Color(153, 153, 153));
        loc26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc26.setText("Pluto");
        loc26.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc26.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc26.setOpaque(true);
        jLayeredPane2.add(loc26);
        loc26.setBounds(80, 510, 50, 50);

        sd2.setBackground(new java.awt.Color(51, 51, 51));
        sd2.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd2.setForeground(new java.awt.Color(0, 0, 0));
        sd2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd2.setOpaque(true);
        sd2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd2MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd2);
        sd2.setBounds(130, 20, 50, 20);

        sd6.setBackground(new java.awt.Color(51, 51, 51));
        sd6.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd6.setForeground(new java.awt.Color(0, 0, 0));
        sd6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd6.setOpaque(true);
        sd6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd6MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd6);
        sd6.setBounds(330, 20, 50, 20);

        sd17.setBackground(new java.awt.Color(51, 51, 51));
        sd17.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd17.setForeground(new java.awt.Color(0, 0, 0));
        sd17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd17.setOpaque(true);
        sd17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd17MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd17);
        sd17.setBounds(480, 440, 20, 50);

        sd10.setBackground(new java.awt.Color(51, 51, 51));
        sd10.setFont(new java.awt.Font("Arial Black", 0, 11)); // NOI18N
        sd10.setForeground(new java.awt.Color(0, 0, 0));
        sd10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd10.setOpaque(true);
        sd10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd10MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd10);
        sd10.setBounds(480, 90, 20, 50);

        sd13.setBackground(new java.awt.Color(51, 51, 51));
        sd13.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd13.setForeground(new java.awt.Color(0, 0, 0));
        sd13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd13.setOpaque(true);
        sd13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd13MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd13);
        sd13.setBounds(480, 240, 20, 50);

        sd7.setBackground(new java.awt.Color(51, 51, 51));
        sd7.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd7.setForeground(new java.awt.Color(0, 0, 0));
        sd7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd7.setOpaque(true);
        sd7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd7MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd7);
        sd7.setBounds(380, 20, 50, 20);

        sd31.setBackground(new java.awt.Color(51, 51, 51));
        sd31.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd31.setForeground(new java.awt.Color(0, 0, 0));
        sd31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd31.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd31.setOpaque(true);
        sd31.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd31MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd31);
        sd31.setBounds(10, 290, 20, 50);

        sd19.setBackground(new java.awt.Color(51, 51, 51));
        sd19.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd19.setForeground(new java.awt.Color(0, 0, 0));
        sd19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd19.setOpaque(true);
        sd19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd19MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd19);
        sd19.setBounds(430, 490, 50, 20);

        sd1.setBackground(new java.awt.Color(51, 51, 51));
        sd1.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd1.setForeground(new java.awt.Color(0, 0, 0));
        sd1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd1.setFocusable(false);
        sd1.setOpaque(true);
        sd1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd1MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd1);
        sd1.setBounds(80, 20, 50, 20);

        sd22.setBackground(new java.awt.Color(51, 51, 51));
        sd22.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd22.setForeground(new java.awt.Color(0, 0, 0));
        sd22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd22.setOpaque(true);
        sd22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd22MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd22);
        sd22.setBounds(280, 490, 50, 20);

        sd25.setBackground(new java.awt.Color(51, 51, 51));
        sd25.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd25.setForeground(new java.awt.Color(0, 0, 0));
        sd25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd25.setOpaque(true);
        sd25.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd25MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd25);
        sd25.setBounds(130, 490, 50, 20);

        sd24.setBackground(new java.awt.Color(51, 51, 51));
        sd24.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd24.setForeground(new java.awt.Color(0, 0, 0));
        sd24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd24.setOpaque(true);
        sd24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd24MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd24);
        sd24.setBounds(180, 490, 50, 20);

        sd28.setBackground(new java.awt.Color(51, 51, 51));
        sd28.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd28.setForeground(new java.awt.Color(0, 0, 0));
        sd28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd28.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd28.setOpaque(true);
        sd28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd28MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd28);
        sd28.setBounds(10, 440, 20, 50);

        sd35.setBackground(new java.awt.Color(51, 51, 51));
        sd35.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd35.setForeground(new java.awt.Color(0, 0, 0));
        sd35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd35.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd35.setOpaque(true);
        sd35.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd35MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd35);
        sd35.setBounds(10, 90, 20, 50);

        sd8.setBackground(new java.awt.Color(51, 51, 51));
        sd8.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd8.setForeground(new java.awt.Color(0, 0, 0));
        sd8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd8.setOpaque(true);
        sd8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd8MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd8);
        sd8.setBounds(430, 20, 50, 20);

        sd4.setBackground(new java.awt.Color(51, 51, 51));
        sd4.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd4.setForeground(new java.awt.Color(0, 0, 0));
        sd4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd4.setOpaque(true);
        sd4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd4MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd4);
        sd4.setBounds(230, 20, 50, 20);

        sd15.setBackground(new java.awt.Color(51, 51, 51));
        sd15.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd15.setForeground(new java.awt.Color(0, 0, 0));
        sd15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd15.setOpaque(true);
        sd15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd15MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd15);
        sd15.setBounds(480, 340, 20, 50);

        sd11.setBackground(new java.awt.Color(51, 51, 51));
        sd11.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd11.setForeground(new java.awt.Color(0, 0, 0));
        sd11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd11.setOpaque(true);
        sd11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd11MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd11);
        sd11.setBounds(480, 140, 20, 50);

        sd20.setBackground(new java.awt.Color(51, 51, 51));
        sd20.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd20.setForeground(new java.awt.Color(0, 0, 0));
        sd20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd20.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd20.setOpaque(true);
        sd20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd20MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd20);
        sd20.setBounds(380, 490, 50, 20);

        sd29.setBackground(new java.awt.Color(51, 51, 51));
        sd29.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd29.setForeground(new java.awt.Color(0, 0, 0));
        sd29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd29.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd29.setOpaque(true);
        sd29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd29MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd29);
        sd29.setBounds(10, 390, 20, 50);

        sd33.setBackground(new java.awt.Color(51, 51, 51));
        sd33.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd33.setForeground(new java.awt.Color(0, 0, 0));
        sd33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd33.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd33.setOpaque(true);
        sd33.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sd33MouseClicked(evt);
            }
        });
        jLayeredPane2.add(sd33);
        sd33.setBounds(10, 190, 20, 50);

        loc27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/free1.jpg"))); // NOI18N
        loc27.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc27.setOpaque(true);
        jLayeredPane2.add(loc27);
        loc27.setBounds(10, 490, 70, 70);

        loc1.setBackground(new java.awt.Color(51, 51, 51));
        loc1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc1.setForeground(new java.awt.Color(153, 153, 153));
        loc1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc1.setText("Mercury");
        loc1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc1.setOpaque(true);
        jLayeredPane2.add(loc1);
        loc1.setBounds(80, 40, 50, 50);

        loc7.setBackground(new java.awt.Color(51, 51, 51));
        loc7.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc7.setForeground(new java.awt.Color(153, 153, 153));
        loc7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc7.setText("Phobos");
        loc7.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc7.setOpaque(true);
        jLayeredPane2.add(loc7);
        loc7.setBounds(380, 40, 50, 50);

        loc2.setBackground(new java.awt.Color(51, 51, 51));
        loc2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc2.setForeground(new java.awt.Color(153, 153, 153));
        loc2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc2.setText("Venus");
        loc2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc2.setOpaque(true);
        jLayeredPane2.add(loc2);
        loc2.setBounds(130, 40, 50, 50);

        loc3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/casino_1.jpg"))); // NOI18N
        loc3.setToolTipText("");
        loc3.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc3.setOpaque(true);
        jLayeredPane2.add(loc3);
        loc3.setBounds(180, 20, 50, 70);

        loc5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/train.jpg"))); // NOI18N
        loc5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc5.setOpaque(true);
        jLayeredPane2.add(loc5);
        loc5.setBounds(280, 20, 50, 70);

        loc4.setBackground(new java.awt.Color(51, 51, 51));
        loc4.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc4.setForeground(new java.awt.Color(153, 153, 153));
        loc4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc4.setText("Moon");
        loc4.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc4.setOpaque(true);
        jLayeredPane2.add(loc4);
        loc4.setBounds(230, 40, 50, 50);

        loc6.setBackground(new java.awt.Color(51, 51, 51));
        loc6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc6.setForeground(new java.awt.Color(153, 153, 153));
        loc6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc6.setText("Mars");
        loc6.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc6.setOpaque(true);
        jLayeredPane2.add(loc6);
        loc6.setBounds(330, 40, 50, 50);

        loc10.setBackground(new java.awt.Color(51, 51, 51));
        loc10.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc10.setForeground(new java.awt.Color(153, 153, 153));
        loc10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc10.setText("Jupiter");
        loc10.setToolTipText("");
        loc10.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc10.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        loc10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        loc10.setOpaque(true);
        jLayeredPane2.add(loc10);
        loc10.setBounds(500, 90, 50, 50);

        loc11.setBackground(new java.awt.Color(51, 51, 51));
        loc11.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc11.setForeground(new java.awt.Color(153, 153, 153));
        loc11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc11.setText("Io");
        loc11.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc11.setOpaque(true);
        jLayeredPane2.add(loc11);
        loc11.setBounds(500, 140, 50, 50);

        loc13.setBackground(new java.awt.Color(51, 51, 51));
        loc13.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc13.setForeground(new java.awt.Color(153, 153, 153));
        loc13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc13.setText("Europa");
        loc13.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc13.setOpaque(true);
        jLayeredPane2.add(loc13);
        loc13.setBounds(500, 240, 50, 50);

        loc9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/free1.jpg"))); // NOI18N
        loc9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc9.setOpaque(true);
        jLayeredPane2.add(loc9);
        loc9.setBounds(480, 20, 70, 70);

        loc14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/train_2.jpg"))); // NOI18N
        loc14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc14.setOpaque(true);
        jLayeredPane2.add(loc14);
        loc14.setBounds(480, 290, 70, 50);

        loc19.setBackground(new java.awt.Color(51, 51, 51));
        loc19.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc19.setForeground(new java.awt.Color(153, 153, 153));
        loc19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc19.setText("Uranus");
        loc19.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc19.setOpaque(true);
        jLayeredPane2.add(loc19);
        loc19.setBounds(430, 510, 50, 50);

        loc8.setBackground(new java.awt.Color(51, 51, 51));
        loc8.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc8.setForeground(new java.awt.Color(153, 153, 153));
        loc8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc8.setText("Deimos");
        loc8.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc8.setOpaque(true);
        jLayeredPane2.add(loc8);
        loc8.setBounds(430, 40, 50, 50);

        loc21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/casino_1.jpg"))); // NOI18N
        loc21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc21.setOpaque(true);
        jLayeredPane2.add(loc21);
        loc21.setBounds(330, 490, 50, 70);

        loc22.setBackground(new java.awt.Color(51, 51, 51));
        loc22.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc22.setForeground(new java.awt.Color(153, 153, 153));
        loc22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc22.setText("Ariel");
        loc22.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc22.setOpaque(true);
        jLayeredPane2.add(loc22);
        loc22.setBounds(280, 510, 50, 50);

        loc23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/train.jpg"))); // NOI18N
        loc23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc23.setOpaque(true);
        jLayeredPane2.add(loc23);
        loc23.setBounds(230, 490, 50, 70);

        loc25.setBackground(new java.awt.Color(51, 51, 51));
        loc25.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc25.setForeground(new java.awt.Color(153, 153, 153));
        loc25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc25.setText("Triton");
        loc25.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc25.setOpaque(true);
        jLayeredPane2.add(loc25);
        loc25.setBounds(130, 510, 50, 50);

        loc24.setBackground(new java.awt.Color(51, 51, 51));
        loc24.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc24.setForeground(new java.awt.Color(153, 153, 153));
        loc24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc24.setText("Neptune");
        loc24.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc24.setOpaque(true);
        jLayeredPane2.add(loc24);
        loc24.setBounds(180, 510, 50, 50);

        loc18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/jail.jpg"))); // NOI18N
        loc18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc18.setOpaque(true);
        jLayeredPane2.add(loc18);
        loc18.setBounds(480, 490, 70, 70);

        loc17.setBackground(new java.awt.Color(51, 51, 51));
        loc17.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc17.setForeground(new java.awt.Color(153, 153, 153));
        loc17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc17.setText("Titan");
        loc17.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc17.setOpaque(true);
        jLayeredPane2.add(loc17);
        loc17.setBounds(500, 440, 50, 50);

        loc29.setBackground(new java.awt.Color(51, 51, 51));
        loc29.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc29.setForeground(new java.awt.Color(153, 153, 153));
        loc29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc29.setText("Acloria");
        loc29.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc29.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc29.setOpaque(true);
        jLayeredPane2.add(loc29);
        loc29.setBounds(30, 390, 50, 50);

        loc31.setBackground(new java.awt.Color(51, 51, 51));
        loc31.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc31.setForeground(new java.awt.Color(153, 153, 153));
        loc31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc31.setText("Sudrea");
        loc31.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc31.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc31.setOpaque(true);
        jLayeredPane2.add(loc31);
        loc31.setBounds(30, 290, 50, 50);

        loc28.setBackground(new java.awt.Color(51, 51, 51));
        loc28.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc28.setForeground(new java.awt.Color(153, 153, 153));
        loc28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc28.setText("Skore");
        loc28.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc28.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc28.setOpaque(true);
        jLayeredPane2.add(loc28);
        loc28.setBounds(30, 440, 50, 50);

        loc32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/train_2.jpg"))); // NOI18N
        loc32.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc32.setOpaque(true);
        jLayeredPane2.add(loc32);
        loc32.setBounds(10, 240, 70, 50);

        loc30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/casino_2.jpg"))); // NOI18N
        loc30.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc30.setOpaque(true);
        jLayeredPane2.add(loc30);
        loc30.setBounds(10, 340, 70, 50);

        loc33.setBackground(new java.awt.Color(51, 51, 51));
        loc33.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc33.setForeground(new java.awt.Color(153, 153, 153));
        loc33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc33.setText("Tathor");
        loc33.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc33.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc33.setOpaque(true);
        jLayeredPane2.add(loc33);
        loc33.setBounds(30, 190, 50, 50);

        loc34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/job.jpg"))); // NOI18N
        loc34.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc34.setOpaque(true);
        jLayeredPane2.add(loc34);
        loc34.setBounds(10, 140, 70, 50);

        sd5.setBackground(new java.awt.Color(0, 153, 153));
        sd5.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd5.setForeground(new java.awt.Color(255, 255, 255));
        sd5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd5.setOpaque(true);
        jLayeredPane2.add(sd5);
        sd5.setBounds(280, 20, 50, 20);

        sd14.setBackground(new java.awt.Color(204, 0, 102));
        sd14.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd14.setForeground(new java.awt.Color(255, 255, 255));
        sd14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd14.setOpaque(true);
        jLayeredPane2.add(sd14);
        sd14.setBounds(480, 290, 20, 50);

        sd21.setBackground(new java.awt.Color(255, 0, 0));
        sd21.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd21.setForeground(new java.awt.Color(255, 255, 255));
        sd21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd21.setOpaque(true);
        jLayeredPane2.add(sd21);
        sd21.setBounds(330, 490, 50, 20);

        sd36.setBackground(new java.awt.Color(51, 153, 0));
        sd36.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd36.setForeground(new java.awt.Color(255, 255, 255));
        sd36.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLayeredPane2.add(sd36);
        sd36.setBounds(10, 20, 20, 50);

        sd32.setBackground(new java.awt.Color(0, 0, 255));
        sd32.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd32.setForeground(new java.awt.Color(255, 255, 255));
        sd32.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd32.setOpaque(true);
        jLayeredPane2.add(sd32);
        sd32.setBounds(60, 240, 20, 50);

        loc36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/start.jpg"))); // NOI18N
        loc36.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc36.setOpaque(true);
        jLayeredPane2.add(loc36);
        loc36.setBounds(10, 20, 70, 70);

        loc12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/casino_2.jpg"))); // NOI18N
        loc12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc12.setOpaque(true);
        jLayeredPane2.add(loc12);
        loc12.setBounds(480, 190, 70, 50);

        Name5.setBackground(new java.awt.Color(51, 51, 51));
        Name5.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Name5.setForeground(new java.awt.Color(159, 181, 193));
        Name5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Name5.setText("Player5");
        jLayeredPane2.add(Name5);
        Name5.setBounds(850, 300, 120, 49);

        Name6.setBackground(new java.awt.Color(51, 51, 51));
        Name6.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Name6.setForeground(new java.awt.Color(159, 181, 193));
        Name6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Name6.setText("Player6");
        jLayeredPane2.add(Name6);
        Name6.setBounds(850, 370, 120, 49);

        Name7.setBackground(new java.awt.Color(51, 51, 51));
        Name7.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Name7.setForeground(new java.awt.Color(159, 181, 193));
        Name7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Name7.setText("Player7");
        jLayeredPane2.add(Name7);
        Name7.setBounds(850, 440, 120, 49);

        Money5.setBackground(new java.awt.Color(51, 51, 51));
        Money5.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Money5.setForeground(new java.awt.Color(159, 181, 193));
        Money5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Money5.setText("-");
        jLayeredPane2.add(Money5);
        Money5.setBounds(1000, 300, 120, 49);

        Money6.setBackground(new java.awt.Color(51, 51, 51));
        Money6.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Money6.setForeground(new java.awt.Color(159, 181, 193));
        Money6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Money6.setText("-");
        jLayeredPane2.add(Money6);
        Money6.setBounds(1000, 370, 120, 49);

        Money4.setBackground(new java.awt.Color(51, 51, 51));
        Money4.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Money4.setForeground(new java.awt.Color(159, 181, 193));
        Money4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Money4.setText("-");
        jLayeredPane2.add(Money4);
        Money4.setBounds(1000, 230, 120, 49);

        Money7.setBackground(new java.awt.Color(51, 51, 51));
        Money7.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Money7.setForeground(new java.awt.Color(159, 181, 193));
        Money7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Money7.setText("-");
        jLayeredPane2.add(Money7);
        Money7.setBounds(1000, 440, 120, 49);

        Name4.setBackground(new java.awt.Color(51, 51, 51));
        Name4.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Name4.setForeground(new java.awt.Color(159, 181, 193));
        Name4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Name4.setText("Player4");
        jLayeredPane2.add(Name4);
        Name4.setBounds(850, 230, 120, 49);

        Name3.setBackground(new java.awt.Color(51, 51, 51));
        Name3.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Name3.setForeground(new java.awt.Color(159, 181, 193));
        Name3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Name3.setText("Player3");
        jLayeredPane2.add(Name3);
        Name3.setBounds(850, 160, 120, 49);

        loc20.setBackground(new java.awt.Color(51, 51, 51));
        loc20.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc20.setForeground(new java.awt.Color(153, 153, 153));
        loc20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc20.setText("Oberon");
        loc20.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc20.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc20.setOpaque(true);
        jLayeredPane2.add(loc20);
        loc20.setBounds(380, 510, 50, 50);

        loc16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/job.jpg"))); // NOI18N
        loc16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc16.setOpaque(true);
        jLayeredPane2.add(loc16);
        loc16.setBounds(480, 390, 70, 50);

        loc15.setBackground(new java.awt.Color(51, 51, 51));
        loc15.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc15.setForeground(new java.awt.Color(153, 153, 153));
        loc15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc15.setText("Saturn");
        loc15.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc15.setOpaque(true);
        jLayeredPane2.add(loc15);
        loc15.setBounds(500, 340, 50, 50);

        loc35.setBackground(new java.awt.Color(51, 51, 51));
        loc35.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        loc35.setForeground(new java.awt.Color(153, 153, 153));
        loc35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loc35.setText("Thiea");
        loc35.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        loc35.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        loc35.setOpaque(true);
        jLayeredPane2.add(loc35);
        loc35.setBounds(30, 90, 50, 50);

        sd3.setBackground(new java.awt.Color(0, 153, 153));
        sd3.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd3.setForeground(new java.awt.Color(255, 255, 255));
        sd3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd3.setOpaque(true);
        jLayeredPane2.add(sd3);
        sd3.setBounds(180, 20, 50, 20);

        sd16.setBackground(new java.awt.Color(204, 0, 102));
        sd16.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd16.setForeground(new java.awt.Color(255, 255, 255));
        sd16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd16.setOpaque(true);
        jLayeredPane2.add(sd16);
        sd16.setBounds(480, 390, 20, 50);

        sd12.setBackground(new java.awt.Color(204, 0, 102));
        sd12.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd12.setForeground(new java.awt.Color(255, 255, 255));
        sd12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd12.setOpaque(true);
        jLayeredPane2.add(sd12);
        sd12.setBounds(480, 190, 20, 50);

        sd23.setBackground(new java.awt.Color(255, 0, 0));
        sd23.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd23.setForeground(new java.awt.Color(255, 255, 255));
        sd23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd23.setOpaque(true);
        jLayeredPane2.add(sd23);
        sd23.setBounds(230, 490, 50, 20);

        sd30.setBackground(new java.awt.Color(51, 153, 0));
        sd30.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd30.setForeground(new java.awt.Color(255, 255, 255));
        sd30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd30.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd30.setOpaque(true);
        jLayeredPane2.add(sd30);
        sd30.setBounds(60, 340, 20, 50);

        sd9.setBackground(new java.awt.Color(51, 153, 0));
        sd9.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd9.setForeground(new java.awt.Color(255, 255, 255));
        sd9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd9.setOpaque(true);
        jLayeredPane2.add(sd9);
        sd9.setBounds(490, 40, 20, 50);

        sd18.setBackground(new java.awt.Color(51, 153, 0));
        sd18.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd18.setForeground(new java.awt.Color(255, 255, 255));
        sd18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd18.setOpaque(true);
        jLayeredPane2.add(sd18);
        sd18.setBounds(530, 510, 20, 50);

        sd27.setBackground(new java.awt.Color(51, 153, 0));
        sd27.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd27.setForeground(new java.awt.Color(255, 255, 255));
        sd27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd27.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd27.setOpaque(true);
        jLayeredPane2.add(sd27);
        sd27.setBounds(40, 480, 20, 50);

        sd34.setBackground(new java.awt.Color(51, 153, 0));
        sd34.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        sd34.setForeground(new java.awt.Color(255, 255, 255));
        sd34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sd34.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sd34.setOpaque(true);
        jLayeredPane2.add(sd34);
        sd34.setBounds(60, 140, 20, 50);

        zar2.setBackground(new java.awt.Color(51, 51, 51));
        zar2.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        zar2.setForeground(new java.awt.Color(255, 255, 255));
        zar2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        zar2.setText("-");
        jLayeredPane2.add(zar2);
        zar2.setBounds(690, 360, 50, 50);

        info.setBackground(new java.awt.Color(51, 51, 51));
        info.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        info.setForeground(new java.awt.Color(255, 255, 255));
        info.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        info.setText("Info");
        info.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLayeredPane2.add(info);
        info.setBounds(580, 100, 250, 320);

        infosell.setBackground(new java.awt.Color(51, 51, 51));
        infosell.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        infosell.setForeground(new java.awt.Color(255, 255, 255));
        infosell.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        infosell.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLayeredPane2.add(infosell);
        infosell.setBounds(580, 100, 250, 320);

        quit.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        quit.setForeground(new java.awt.Color(159, 181, 193));
        quit.setText("Quit");
        quit.setBorderPainted(false);
        quit.setContentAreaFilled(false);
        quit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitActionPerformed(evt);
            }
        });
        jLayeredPane2.add(quit);
        quit.setBounds(220, 390, 120, 50);

        jLabel_reset1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_small.png"))); // NOI18N
        jLabel_reset1.setText("jLabel2");
        jLayeredPane2.add(jLabel_reset1);
        jLabel_reset1.setBounds(220, 380, 130, 70);

        jLabel_reset2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_small.png"))); // NOI18N
        jLabel_reset2.setText("jLabel2");
        jLayeredPane2.add(jLabel_reset2);
        jLabel_reset2.setBounds(220, 270, 130, 50);

        Reset.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Reset.setForeground(new java.awt.Color(159, 181, 193));
        Reset.setText("Reset");
        Reset.setBorderPainted(false);
        Reset.setContentAreaFilled(false);
        Reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ResetActionPerformed(evt);
            }
        });
        jLayeredPane2.add(Reset);
        Reset.setBounds(220, 270, 120, 50);

        jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_small_background.png"))); // NOI18N
        jLabel24.setText("jLabel22");
        jLayeredPane2.add(jLabel24);
        jLabel24.setBounds(220, 270, 130, 50);

        jLabel38.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_small_background.png"))); // NOI18N
        jLabel38.setText("jLabel22");
        jLayeredPane2.add(jLabel38);
        jLabel38.setBounds(220, 390, 130, 50);

        jLabel_reset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_small.png"))); // NOI18N
        jLabel_reset.setText("jLabel2");
        jLayeredPane2.add(jLabel_reset);
        jLabel_reset.setBounds(220, 330, 130, 50);

        menu.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        menu.setForeground(new java.awt.Color(159, 181, 193));
        menu.setText("Menu");
        menu.setBorderPainted(false);
        menu.setContentAreaFilled(false);
        menu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuActionPerformed(evt);
            }
        });
        jLayeredPane2.add(menu);
        menu.setBounds(220, 330, 120, 50);

        Name1.setBackground(new java.awt.Color(51, 51, 51));
        Name1.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Name1.setForeground(new java.awt.Color(159, 181, 193));
        Name1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Name1.setText("Player1");
        Name1.setToolTipText("");
        Name1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        Name1.setDoubleBuffered(true);
        jLayeredPane2.add(Name1);
        Name1.setBounds(850, 20, 120, 50);

        jLabel39.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_small_background.png"))); // NOI18N
        jLabel39.setText("jLabel22");
        jLayeredPane2.add(jLabel39);
        jLabel39.setBounds(220, 330, 130, 50);

        Money2.setBackground(new java.awt.Color(51, 51, 51));
        Money2.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Money2.setForeground(new java.awt.Color(159, 181, 193));
        Money2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Money2.setText("-");
        jLayeredPane2.add(Money2);
        Money2.setBounds(1000, 90, 120, 49);

        Money1.setBackground(new java.awt.Color(51, 51, 51));
        Money1.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Money1.setForeground(new java.awt.Color(159, 181, 193));
        Money1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Money1.setText("-");
        jLayeredPane2.add(Money1);
        Money1.setBounds(1000, 20, 120, 49);

        rachetuta.setAlignmentX(0.1F);
        rachetuta.setAlignmentY(0.1F);

        rac_p2_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p2_3);
        rac_p2_3.setBounds(30, 95, 20, 20);

        rac_p2_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p2_2);
        rac_p2_2.setBounds(30, 85, 20, 20);

        rac_p2_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p2_1);
        rac_p2_1.setBounds(30, 75, 20, 20);

        rac_p3_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p3_3);
        rac_p3_3.setBounds(30, 165, 20, 20);

        rac_p3_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p3_2);
        rac_p3_2.setBounds(30, 155, 20, 20);

        rac_p3_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p3_1);
        rac_p3_1.setBounds(30, 145, 20, 20);

        rac_p4_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p4_3);
        rac_p4_3.setBounds(30, 235, 20, 20);

        rac_p4_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p4_2);
        rac_p4_2.setBounds(30, 225, 20, 20);

        rac_p4_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p4_1);
        rac_p4_1.setBounds(30, 215, 20, 20);

        rac_p5_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p5_3);
        rac_p5_3.setBounds(30, 305, 30, 20);

        rac_p5_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p5_2);
        rac_p5_2.setBounds(30, 295, 30, 20);

        rac_p5_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p5_1);
        rac_p5_1.setBounds(30, 285, 30, 20);

        rac_p6_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p6_3);
        rac_p6_3.setBounds(30, 375, 30, 20);

        rac_p6_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p6_2);
        rac_p6_2.setBounds(30, 365, 30, 20);

        rac_p6_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p6_1);
        rac_p6_1.setBounds(30, 355, 30, 20);

        rac_p7_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p7_3);
        rac_p7_3.setBounds(30, 445, 30, 20);

        rac_p7_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p7_2);
        rac_p7_2.setBounds(30, 435, 30, 20);

        rac_p7_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p7_1);
        rac_p7_1.setBounds(30, 425, 13, 20);

        rac_p8_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p8_3);
        rac_p8_3.setBounds(30, 515, 30, 20);

        rac_p8_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p8_2);
        rac_p8_2.setBounds(30, 505, 30, 20);

        rac_p8_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rachetuta.add(rac_p8_1);
        rac_p8_1.setBounds(30, 495, 30, 20);

        rac_p1_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rac_p1_3.setAlignmentY(0.1F);
        rachetuta.add(rac_p1_3);
        rac_p1_3.setBounds(30, 25, 20, 20);

        rac_p1_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rac_p1_2.setAlignmentY(0.1F);
        rachetuta.add(rac_p1_2);
        rac_p1_2.setBounds(30, 15, 20, 20);

        rac_p1_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/cursor.png"))); // NOI18N
        rac_p1_1.setAlignmentY(0.1F);
        rachetuta.add(rac_p1_1);
        rac_p1_1.setBounds(30, 5, 20, 20);

        PlayerGlow1.setBackground(new java.awt.Color(51, 51, 51));
        PlayerGlow1.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        PlayerGlow1.setForeground(new java.awt.Color(255, 0, 1));
        PlayerGlow1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PlayerGlow1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion1.png"))); // NOI18N
        rachetuta.add(PlayerGlow1);
        PlayerGlow1.setBounds(10, 10, 20, 30);

        PlayerGlow2.setBackground(new java.awt.Color(51, 51, 51));
        PlayerGlow2.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        PlayerGlow2.setForeground(new java.awt.Color(255, 0, 1));
        PlayerGlow2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PlayerGlow2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion2.png"))); // NOI18N
        rachetuta.add(PlayerGlow2);
        PlayerGlow2.setBounds(10, 80, 20, 30);

        PlayerGlow3.setBackground(new java.awt.Color(51, 51, 51));
        PlayerGlow3.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        PlayerGlow3.setForeground(new java.awt.Color(255, 0, 1));
        PlayerGlow3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PlayerGlow3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion3.png"))); // NOI18N
        rachetuta.add(PlayerGlow3);
        PlayerGlow3.setBounds(10, 150, 20, 30);

        PlayerGlow4.setBackground(new java.awt.Color(51, 51, 51));
        PlayerGlow4.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        PlayerGlow4.setForeground(new java.awt.Color(255, 0, 1));
        PlayerGlow4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PlayerGlow4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion4.png"))); // NOI18N
        rachetuta.add(PlayerGlow4);
        PlayerGlow4.setBounds(10, 220, 20, 30);

        PlayerGlow5.setBackground(new java.awt.Color(51, 51, 51));
        PlayerGlow5.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        PlayerGlow5.setForeground(new java.awt.Color(255, 0, 1));
        PlayerGlow5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PlayerGlow5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion5.png"))); // NOI18N
        rachetuta.add(PlayerGlow5);
        PlayerGlow5.setBounds(10, 290, 20, 30);

        PlayerGlow6.setBackground(new java.awt.Color(51, 51, 51));
        PlayerGlow6.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        PlayerGlow6.setForeground(new java.awt.Color(255, 0, 1));
        PlayerGlow6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PlayerGlow6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion6.png"))); // NOI18N
        rachetuta.add(PlayerGlow6);
        PlayerGlow6.setBounds(10, 360, 20, 30);

        PlayerGlow7.setBackground(new java.awt.Color(51, 51, 51));
        PlayerGlow7.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        PlayerGlow7.setForeground(new java.awt.Color(255, 0, 1));
        PlayerGlow7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PlayerGlow7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion7.png"))); // NOI18N
        rachetuta.add(PlayerGlow7);
        PlayerGlow7.setBounds(10, 430, 20, 30);

        PlayerGlow8.setBackground(new java.awt.Color(51, 51, 51));
        PlayerGlow8.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        PlayerGlow8.setForeground(new java.awt.Color(255, 0, 1));
        PlayerGlow8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PlayerGlow8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/pion8.png"))); // NOI18N
        rachetuta.add(PlayerGlow8);
        PlayerGlow8.setBounds(10, 500, 20, 30);

        Player_list.add(rachetuta);
        rachetuta.setBounds(110, 20, 50, 540);

        jLabel42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium.png"))); // NOI18N
        jLabel42.setText("jLabel19");
        Player_list.add(jLabel42);
        jLabel42.setBounds(0, 20, 270, 50);

        jLabel43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium_background.png"))); // NOI18N
        Player_list.add(jLabel43);
        jLabel43.setBounds(0, 20, 270, 50);

        jLabel44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium.png"))); // NOI18N
        jLabel44.setText("jLabel19");
        Player_list.add(jLabel44);
        jLabel44.setBounds(0, 90, 270, 50);

        jLabel45.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium_background.png"))); // NOI18N
        Player_list.add(jLabel45);
        jLabel45.setBounds(0, 90, 270, 50);

        jLabel46.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium.png"))); // NOI18N
        jLabel46.setText("jLabel19");
        Player_list.add(jLabel46);
        jLabel46.setBounds(0, 160, 270, 50);

        jLabel47.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium_background.png"))); // NOI18N
        Player_list.add(jLabel47);
        jLabel47.setBounds(0, 160, 270, 50);

        jLabel48.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium.png"))); // NOI18N
        jLabel48.setText("jLabel19");
        Player_list.add(jLabel48);
        jLabel48.setBounds(0, 230, 270, 50);

        jLabel49.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium_background.png"))); // NOI18N
        Player_list.add(jLabel49);
        jLabel49.setBounds(0, 230, 270, 50);

        jLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium.png"))); // NOI18N
        jLabel50.setText("jLabel19");
        Player_list.add(jLabel50);
        jLabel50.setBounds(0, 300, 270, 50);

        jLabel51.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium_background.png"))); // NOI18N
        Player_list.add(jLabel51);
        jLabel51.setBounds(0, 300, 270, 50);

        jLabel52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium.png"))); // NOI18N
        jLabel52.setText("jLabel19");
        Player_list.add(jLabel52);
        jLabel52.setBounds(0, 370, 270, 50);

        jLabel53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium_background.png"))); // NOI18N
        Player_list.add(jLabel53);
        jLabel53.setBounds(0, 370, 270, 50);

        jLabel54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium.png"))); // NOI18N
        jLabel54.setText("jLabel19");
        Player_list.add(jLabel54);
        jLabel54.setBounds(0, 440, 270, 50);

        jLabel55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium_background.png"))); // NOI18N
        Player_list.add(jLabel55);
        jLabel55.setBounds(0, 440, 270, 50);

        jLabel56.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium.png"))); // NOI18N
        jLabel56.setText("jLabel19");
        Player_list.add(jLabel56);
        jLabel56.setBounds(0, 510, 270, 50);

        jLabel57.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_medium_background.png"))); // NOI18N
        Player_list.add(jLabel57);
        jLabel57.setBounds(0, 510, 270, 50);

        jLayeredPane2.add(Player_list);
        Player_list.setBounds(850, 0, 280, 580);

        jPanel1.add(jLayeredPane2);
        jLayeredPane2.setBounds(0, 0, 1130, 590);

        metal_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/metal.png"))); // NOI18N
        metal_label.setText("jLabel38");
        jPanel1.add(metal_label);
        metal_label.setBounds(0, 0, 860, 590);

        background_label.setBackground(new java.awt.Color(255, 0, 0));
        background_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/background.jpg"))); // NOI18N
        background_label.setText("jLabel1");
        background_label.setOpaque(true);
        jPanel1.add(background_label);
        background_label.setBounds(0, 0, 1130, 680);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1130, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    public void initialization(){
        
        
        //<editor-fold defaultstate="collapsed" desc="HideElements">
        Sell.setVisible(false);
        jRent.setVisible(false);
        Pion1.setVisible(false);
        Pion2.setVisible(false);
        Pion3.setVisible(false);
        Pion4.setVisible(false);
        Pion5.setVisible(false);
        Pion6.setVisible(false);
        Pion7.setVisible(false);
        Pion8.setVisible(false);
        
        RoundGlow2.setVisible(false);
        RoundGlow3.setVisible(false);
        RoundGlow4.setVisible(false);
        RoundGlow5.setVisible(false);
        RoundGlow6.setVisible(false);
        RoundGlow7.setVisible(false);
        RoundGlow8.setVisible(false);

        jLabel_roll_off.setVisible(false);
        jLabel_end_on.setVisible(false);

        Buys.setVisible(false);
        Ok.setVisible(false);
        jLabel_ok.setVisible(false);
        Buy.setVisible(false);
        Build.setVisible(false);
        zar1.setVisible(false);
        zar2.setVisible(false);
        Rol_label.setVisible(false);
        
        if (dev!=true)
        {
            DevB1.setVisible(false);
            DevB2.setVisible(false);
            DevB3.setVisible(false);
        }
        
        rac_p1_1.setVisible(false);
        rac_p1_2.setVisible(false);
        rac_p1_3.setVisible(false);
        rac_p2_1.setVisible(false);
        rac_p2_2.setVisible(false);
        rac_p2_3.setVisible(false);
        rac_p3_1.setVisible(false);
        rac_p3_2.setVisible(false);
        rac_p3_3.setVisible(false);
        rac_p4_1.setVisible(false);
        rac_p4_2.setVisible(false);
        rac_p4_3.setVisible(false);
        rac_p5_1.setVisible(false);
        rac_p5_2.setVisible(false);
        rac_p5_3.setVisible(false);
        rac_p6_1.setVisible(false);
        rac_p6_2.setVisible(false);
        rac_p6_3.setVisible(false);
        rac_p7_1.setVisible(false);
        rac_p7_2.setVisible(false);
        rac_p7_3.setVisible(false);
        rac_p8_1.setVisible(false);
        rac_p8_2.setVisible(false);
        rac_p8_3.setVisible(false);
        // </editor-fold>
        
        Player.init();
        Location.init();
        Move.init();
        Chance.init();
        p=0;
        
        //<editor-fold defaultstate="collapsed" desc="ShowPlayerElements">

        for (int i=0;i<n;i++)
        {
            Player.dead[i]=false;
            switch (i) {
                case 0:
                    Money1.setText(Integer.toString(Player.money[i]) + "$");
                    labels[i].setVisible(true);
                    rac_p1_1.setVisible(true);
                    rac_p1_2.setVisible(true);
                    rac_p1_3.setVisible(true);
                    break;
                case 1:
                    Money2.setText(Integer.toString(Player.money[i]) + "$");
                    labels[i].setVisible(true);
                    rac_p2_1.setVisible(true);
                    rac_p2_2.setVisible(true);
                    rac_p2_3.setVisible(true);
                    break;
                case 2:
                    Money3.setText(Integer.toString(Player.money[i]) + "$");
                    labels[i].setVisible(true);
                    rac_p3_1.setVisible(true);
                    rac_p3_2.setVisible(true);
                    rac_p3_3.setVisible(true);
                    break;
                case 3:
                    Money4.setText(Integer.toString(Player.money[i]) + "$");
                    labels[i].setVisible(true);
                    rac_p4_1.setVisible(true);
                    rac_p4_2.setVisible(true);
                    rac_p4_3.setVisible(true);
                    break;
                case 4:
                    Money5.setText(Integer.toString(Player.money[i]) + "$");
                    labels[i].setVisible(true);
                    rac_p5_1.setVisible(true);
                    rac_p5_2.setVisible(true);
                    rac_p5_3.setVisible(true);
                    break;
                case 5:
                    Money6.setText(Integer.toString(Player.money[i]) + "$");
                    labels[i].setVisible(true);
                    rac_p6_1.setVisible(true);
                    rac_p6_2.setVisible(true);
                    rac_p6_3.setVisible(true);
                    break;
                case 6:
                    Money7.setText(Integer.toString(Player.money[i]) + "$");
                    labels[i].setVisible(true);
                    rac_p7_1.setVisible(true);
                    rac_p7_2.setVisible(true);
                    rac_p7_3.setVisible(true);
                    break;
                case 7:
                    Money8.setText(Integer.toString(Player.money[i]) + "$");
                    labels[i].setVisible(true);
                    rac_p8_1.setVisible(true);
                    rac_p8_2.setVisible(true);
                    rac_p8_3.setVisible(true);
                    break;
                default:
                    break;
            }
        }
        
        // </editor-fold>

    }

    private void BuildActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuildActionPerformed

        Build();
        Build.setVisible(false);
        jLabel_ok.setVisible(false);

    }//GEN-LAST:event_BuildActionPerformed

    private void RollActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RollActionPerformed
        
        
        mov.RollAction(evt);
        zar1.setVisible(true);
        zar2.setVisible(true);
        Rol_label.setVisible(true);
   
    }//GEN-LAST:event_RollActionPerformed
    
    private void EndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EndActionPerformed
        
        zar1.setVisible(false);
        zar2.setVisible(false);
        Buy.setVisible(false);
        Build.setVisible(false);
        jLabel_ok.setVisible(false);
        Rol_label.setVisible(false);
        
        if(dice==12 && Player.dead[pion]==false)
        {
            Roll.setEnabled(true);
            jLabel_roll_off.setVisible(false);
            jLabel_roll_on.setVisible(true);
            
            info.setText("<html>" + 
                "<center><p>" + "Announcement:" + "</p></center>" +
                "<center><p>" + "</p></center>" +
                "<center><p>" + "You rolled double 6.Roll again!" + "</p></center>" +
                "</html>");
            
            End.setEnabled(false);
            jLabel_end_off.setVisible(true);
            jLabel_end_on.setVisible(false);
        }
        else
        {
            mov.End();
        }
        
        
    }//GEN-LAST:event_EndActionPerformed

    private void BuyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuyActionPerformed

        Buy(Location.value[location[pion]-1]);
        mov.DisplayBuy();
        
            
    }//GEN-LAST:event_BuyActionPerformed

    private void OkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OkActionPerformed

        Ok.setVisible(false);
        End.setEnabled(true);
        jLabel_end_on.setVisible(true);
        jLabel_end_off.setVisible(false);
        jLabel_ok.setVisible(false);
        change.effect();
        
        Pay(value2pay);
        ShowPlayerInfo(Player.id[pion]);
        value2pay=0;
        mov.DeadCheck();
    }//GEN-LAST:event_OkActionPerformed

    private void quitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_quitActionPerformed

    private void ResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResetActionPerformed
        initialization();
        dispose();
        new Interface().setVisible(true);
    }//GEN-LAST:event_ResetActionPerformed

    private void BuysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuysActionPerformed

        Player.lifes[pion]+=1;
        Pay(5000);
        info.setText("<html>" + 
            "<center><p>" + "Announcement:" + "</p></center>" +
            "<center><p>" + "</p></center>" +
            "<center><p>" + "You have bought a ship." + "</p></center>" +
            "</html>");
        Buys.setVisible(false);
        jLabel_ok.setVisible(false);
    }//GEN-LAST:event_BuysActionPerformed

    private void menuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuActionPerformed
        initialization();
        dispose();
        new Meniu().setVisible(true);
    }//GEN-LAST:event_menuActionPerformed
    
    private void sd8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd8MouseClicked
        if (p!=8)
        {
            p=8;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd8MouseClicked

    private void sd10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd10MouseClicked
        if (p!=10)
        {
            p=10;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd10MouseClicked

    private void sd11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd11MouseClicked
        if (p!=11)
        {
            p=11;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd11MouseClicked

    private void sd13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd13MouseClicked
        if (p!=13)
        {
            p=13;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd13MouseClicked

    private void sd15MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd15MouseClicked
        if (p!=15)
        {
            p=15;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd15MouseClicked

    private void sd17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd17MouseClicked
        if (p!=17)
        {
            p=17;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd17MouseClicked

    private void sd19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd19MouseClicked
        if (p!=19)
        {
            p=19;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd19MouseClicked

    private void sd20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd20MouseClicked
        if (p!=20)
        {
            p=20;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd20MouseClicked

    private void sd22MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd22MouseClicked
        if (p!=22)
        {
            p=22;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd22MouseClicked

    private void sd24MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd24MouseClicked
        if (p!=24)
        {
            p=24;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd24MouseClicked

    private void sd25MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd25MouseClicked
        if (p!=25)
        {
            p=25;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd25MouseClicked

    private void sd26MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd26MouseClicked
        if (p!=26)
        {
            p=26;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd26MouseClicked

    private void sd28MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd28MouseClicked
        if (p!=28)
        {
            p=28;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd28MouseClicked

    private void sd29MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd29MouseClicked
        if (p!=29)
        {
            p=29;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd29MouseClicked

    private void sd31MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd31MouseClicked
        if (p!=31)
        {
            p=31;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd31MouseClicked

    private void sd33MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd33MouseClicked
        if (p!=33)
        {
            p=33;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd33MouseClicked

    private void sd35MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd35MouseClicked
        if (p!=35)
        {
            p=35;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd35MouseClicked

    // </editor-fold>
    
    private void SellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SellActionPerformed
        
        Sell.setVisible(false);
        jLabel_ok.setVisible(false);
        econ.Sell(p);
        
    }//GEN-LAST:event_SellActionPerformed

    private void jRentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRentActionPerformed
        Rent(value2pay);
        
        value2pay=0;
        jRent.setVisible(false);
        End.setEnabled(true);
        jLabel_end_on.setVisible(true);
        jLabel_end_off.setVisible(false);
        jLabel_ok.setVisible(false);
        
    }//GEN-LAST:event_jRentActionPerformed

    private void DevB1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DevB1ActionPerformed
        Player.money[pion]-=1000;
        ShowPlayerInfo(Player.id[pion]);
    }//GEN-LAST:event_DevB1ActionPerformed

    private void DevB2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DevB2ActionPerformed
        Player.lifes[pion]-=1;
        ShowPlayerInfo(Player.id[pion]);
    }//GEN-LAST:event_DevB2ActionPerformed

    private void DevB3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DevB3ActionPerformed
        dice=12;
    }//GEN-LAST:event_DevB3ActionPerformed

    private void sd7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd7MouseClicked
        if (p!=7)
        {
            p=7;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd7MouseClicked

    private void sd6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd6MouseClicked
        if (p!=6)
        {
            p=6;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd6MouseClicked

    private void sd4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd4MouseClicked
        if (p!=4)
        {
            p=4;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd4MouseClicked

    private void sd2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd2MouseClicked
        if (p!=2)
        {
            p=2;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }
    }//GEN-LAST:event_sd2MouseClicked

    //<editor-fold defaultstate="collapsed" desc="LocationSell">
    
    private void sd1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sd1MouseClicked
        if (p!=1)
        {
            p=1;
            DisplaySell(p);
        }
        else
        {
            mov.Return();
        }

    }//GEN-LAST:event_sd1MouseClicked

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new Interface().setVisible(true);
        });
    }
    
    //<editor-fold defaultstate="collapsed" desc="Interface Declarations">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Build;
    public static javax.swing.JButton Buy;
    public static javax.swing.JButton Buys;
    private javax.swing.JButton DevB1;
    private javax.swing.JButton DevB2;
    private javax.swing.JButton DevB3;
    public static javax.swing.JButton End;
    private javax.swing.JLayeredPane Game_Action;
    public static javax.swing.JLabel Money1;
    public static javax.swing.JLabel Money2;
    public static javax.swing.JLabel Money3;
    public static javax.swing.JLabel Money4;
    public static javax.swing.JLabel Money5;
    public static javax.swing.JLabel Money6;
    public static javax.swing.JLabel Money7;
    public static javax.swing.JLabel Money8;
    public static javax.swing.JLabel Name1;
    public static javax.swing.JLabel Name2;
    public static javax.swing.JLabel Name3;
    public static javax.swing.JLabel Name4;
    public static javax.swing.JLabel Name5;
    public static javax.swing.JLabel Name6;
    public static javax.swing.JLabel Name7;
    public static javax.swing.JLabel Name8;
    public static javax.swing.JButton Ok;
    public static javax.swing.JLabel Pion1;
    public static javax.swing.JLabel Pion2;
    public static javax.swing.JLabel Pion3;
    public static javax.swing.JLabel Pion4;
    public static javax.swing.JLabel Pion5;
    public static javax.swing.JLabel Pion6;
    public static javax.swing.JLabel Pion7;
    public static javax.swing.JLabel Pion8;
    public static javax.swing.JLabel PlayerGlow1;
    public static javax.swing.JLabel PlayerGlow2;
    public static javax.swing.JLabel PlayerGlow3;
    public static javax.swing.JLabel PlayerGlow4;
    public static javax.swing.JLabel PlayerGlow5;
    public static javax.swing.JLabel PlayerGlow6;
    public static javax.swing.JLabel PlayerGlow7;
    public static javax.swing.JLabel PlayerGlow8;
    private javax.swing.JLayeredPane Player_list;
    private javax.swing.JButton Reset;
    private javax.swing.JLabel Rol_label;
    public static javax.swing.JButton Roll;
    public static javax.swing.JLabel RoundDisplay;
    public static javax.swing.JLabel RoundGlow1;
    public static javax.swing.JLabel RoundGlow2;
    public static javax.swing.JLabel RoundGlow3;
    public static javax.swing.JLabel RoundGlow4;
    public static javax.swing.JLabel RoundGlow5;
    public static javax.swing.JLabel RoundGlow6;
    public static javax.swing.JLabel RoundGlow7;
    public static javax.swing.JLabel RoundGlow8;
    public static javax.swing.JButton Sell;
    private javax.swing.JLabel background_label;
    public static javax.swing.JLabel info;
    public static javax.swing.JLabel infosell;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    public static javax.swing.JLabel jLabel_end_off;
    public static javax.swing.JLabel jLabel_end_on;
    public static javax.swing.JLabel jLabel_ok;
    public static javax.swing.JLabel jLabel_reset;
    public static javax.swing.JLabel jLabel_reset1;
    public static javax.swing.JLabel jLabel_reset2;
    public static javax.swing.JLabel jLabel_roll_off;
    public static javax.swing.JLabel jLabel_roll_on;
    public static javax.swing.JLayeredPane jLayeredPane1;
    public static javax.swing.JLayeredPane jLayeredPane2;
    private javax.swing.JPanel jPanel1;
    public static javax.swing.JButton jRent;
    public static javax.swing.JLabel loc1;
    public static javax.swing.JLabel loc10;
    protected static javax.swing.JLabel loc11;
    protected static javax.swing.JLabel loc12;
    protected static javax.swing.JLabel loc13;
    public static javax.swing.JLabel loc14;
    public static javax.swing.JLabel loc15;
    public static javax.swing.JLabel loc16;
    public static javax.swing.JLabel loc17;
    public static javax.swing.JLabel loc18;
    public static javax.swing.JLabel loc19;
    public static javax.swing.JLabel loc2;
    public static javax.swing.JLabel loc20;
    public static javax.swing.JLabel loc21;
    public static javax.swing.JLabel loc22;
    protected static javax.swing.JLabel loc23;
    protected static javax.swing.JLabel loc24;
    public static javax.swing.JLabel loc25;
    public static javax.swing.JLabel loc26;
    public static javax.swing.JLabel loc27;
    public static javax.swing.JLabel loc28;
    public static javax.swing.JLabel loc29;
    public static javax.swing.JLabel loc3;
    public static javax.swing.JLabel loc30;
    public static javax.swing.JLabel loc31;
    public static javax.swing.JLabel loc32;
    public static javax.swing.JLabel loc33;
    protected static javax.swing.JLabel loc34;
    public static javax.swing.JLabel loc35;
    public static javax.swing.JLabel loc36;
    public static javax.swing.JLabel loc4;
    public static javax.swing.JLabel loc5;
    public static javax.swing.JLabel loc6;
    public static javax.swing.JLabel loc7;
    public static javax.swing.JLabel loc8;
    public static javax.swing.JLabel loc9;
    private javax.swing.JButton menu;
    private javax.swing.JLabel metal_label;
    private javax.swing.JButton quit;
    public static javax.swing.JLabel rac_p1_1;
    public static javax.swing.JLabel rac_p1_2;
    public static javax.swing.JLabel rac_p1_3;
    public static javax.swing.JLabel rac_p2_1;
    public static javax.swing.JLabel rac_p2_2;
    public static javax.swing.JLabel rac_p2_3;
    public static javax.swing.JLabel rac_p3_1;
    public static javax.swing.JLabel rac_p3_2;
    public static javax.swing.JLabel rac_p3_3;
    public static javax.swing.JLabel rac_p4_1;
    public static javax.swing.JLabel rac_p4_2;
    public static javax.swing.JLabel rac_p4_3;
    public static javax.swing.JLabel rac_p5_1;
    public static javax.swing.JLabel rac_p5_2;
    public static javax.swing.JLabel rac_p5_3;
    public static javax.swing.JLabel rac_p6_1;
    public static javax.swing.JLabel rac_p6_2;
    public static javax.swing.JLabel rac_p6_3;
    public static javax.swing.JLabel rac_p7_1;
    public static javax.swing.JLabel rac_p7_2;
    public static javax.swing.JLabel rac_p7_3;
    public static javax.swing.JLabel rac_p8_1;
    public static javax.swing.JLabel rac_p8_2;
    public static javax.swing.JLabel rac_p8_3;
    private javax.swing.JLayeredPane rachetuta;
    public static javax.swing.JLabel sd1;
    public static javax.swing.JLabel sd10;
    public static javax.swing.JLabel sd11;
    public static javax.swing.JLabel sd12;
    public static javax.swing.JLabel sd13;
    public static javax.swing.JLabel sd14;
    public static javax.swing.JLabel sd15;
    public static javax.swing.JLabel sd16;
    public static javax.swing.JLabel sd17;
    public static javax.swing.JLabel sd18;
    public static javax.swing.JLabel sd19;
    public static javax.swing.JLabel sd2;
    public static javax.swing.JLabel sd20;
    public static javax.swing.JLabel sd21;
    public static javax.swing.JLabel sd22;
    public static javax.swing.JLabel sd23;
    public static javax.swing.JLabel sd24;
    public static javax.swing.JLabel sd25;
    public static javax.swing.JLabel sd26;
    public static javax.swing.JLabel sd27;
    public static javax.swing.JLabel sd28;
    public static javax.swing.JLabel sd29;
    public static javax.swing.JLabel sd3;
    public static javax.swing.JLabel sd30;
    public static javax.swing.JLabel sd31;
    public static javax.swing.JLabel sd32;
    public static javax.swing.JLabel sd33;
    public static javax.swing.JLabel sd34;
    public static javax.swing.JLabel sd35;
    public static javax.swing.JLabel sd36;
    public static javax.swing.JLabel sd4;
    public static javax.swing.JLabel sd5;
    public static javax.swing.JLabel sd6;
    public static javax.swing.JLabel sd7;
    public static javax.swing.JLabel sd8;
    public static javax.swing.JLabel sd9;
    public static javax.swing.JLabel zar1;
    public static javax.swing.JLabel zar2;
    // End of variables declaration//GEN-END:variables

    private void setModalityType(ModalityType modalityType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    // </editor-fold> 
}

