/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.JFormattedTextField;
import javax.swing.JSlider;

/**
 *
 * @author Kocsis
 */
public class ChosePlayer extends javax.swing.JFrame 
{

    public void Cursor()
    {
        Toolkit tool = Toolkit.getDefaultToolkit();
        Image curs = tool.getImage("bin/images/cursor.png");
        Point point = new Point(0,0);
        Cursor cursor = tool.createCustomCursor(curs,point,"Cursor");
        setCursor(cursor);
    }
    
    public ChosePlayer() {
        initComponents();
        Cursor();
    }
    
    public static int n;
    public static boolean dev;
    public static int smoney;
    public static int pmoney;

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        jLabel2 = new javax.swing.JLabel();
        jSlider1 = new javax.swing.JSlider();
        jDev = new javax.swing.JCheckBox();
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.getDefault());
        DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        decimalFormat.setGroupingUsed(false);
        jMoney = new JFormattedTextField(numberFormat);
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jMoneyPass = new JFormattedTextField(numberFormat);
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        menu = new javax.swing.JButton();
        start = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Monopoly Space Adventure");

        jPanel1.setLayout(null);

        jLabel2.setBackground(new java.awt.Color(102, 102, 102));
        jLabel2.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Player Number:");
        jLabel2.setFocusable(false);
        jLabel2.setInheritsPopupMenu(false);
        jLabel2.setRequestFocusEnabled(false);
        jLabel2.setVerifyInputWhenFocusTarget(false);
        jLayeredPane1.add(jLabel2);
        jLabel2.setBounds(0, 10, 240, 40);

        jSlider1.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jSlider1.setMajorTickSpacing(2);
        jSlider1.setMaximum(8);
        jSlider1.setMinimum(2);
        jSlider1.setMinorTickSpacing(1);
        jSlider1.setPaintLabels(true);
        jSlider1.setPaintTicks(true);
        jSlider1.setValue(4);
        jSlider1.setFocusable(false);
        jSlider1.setMaximumSize(new java.awt.Dimension(368, 62));
        jSlider1.setOpaque(false);
        jSlider1.setPreferredSize(new java.awt.Dimension(301, 62));
        jSlider1.setRequestFocusEnabled(false);
        jSlider1.setVerifyInputWhenFocusTarget(false);
        jSlider1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jSlider1MouseClicked(evt);
            }
        });
        jLayeredPane1.add(jSlider1);
        jSlider1.setBounds(0, 50, 301, 70);

        jDev.setFont(new java.awt.Font("Arial Black", 0, 20)); // NOI18N
        jDev.setForeground(new java.awt.Color(0, 0, 0));
        jDev.setText("Enable Development Buttons");
        jDev.setContentAreaFilled(false);
        jDev.setFocusPainted(false);
        jDev.setFocusable(false);
        jLayeredPane1.add(jDev);
        jDev.setBounds(0, 480, 420, 40);

        jMoney.setBackground(new java.awt.Color(255, 255, 255));
        jMoney.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jMoney.setForeground(new java.awt.Color(0, 0, 0));
        jMoney.setText("10000");
        jMoney.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jMoney.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        jMoney.setOpaque(false);
        jLayeredPane1.add(jMoney);
        jMoney.setBounds(0, 170, 170, 40);

        jLabel7.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("$");
        jLayeredPane1.add(jLabel7);
        jLabel7.setBounds(180, 270, 30, 40);

        jLabel8.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Pass Start Money:");
        jLayeredPane1.add(jLabel8);
        jLabel8.setBounds(0, 220, 300, 40);

        jMoneyPass.setBackground(new java.awt.Color(255, 255, 255));
        jMoneyPass.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jMoneyPass.setForeground(new java.awt.Color(0, 0, 0));
        jMoneyPass.setText("1000");
        jMoneyPass.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jMoneyPass.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        jMoneyPass.setOpaque(false);
        jLayeredPane1.add(jMoneyPass);
        jMoneyPass.setBounds(0, 270, 170, 40);

        jLabel10.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setText("$");
        jLayeredPane1.add(jLabel10);
        jLabel10.setBounds(180, 170, 30, 40);

        jLabel11.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("Player Money:");
        jLayeredPane1.add(jLabel11);
        jLabel11.setBounds(0, 120, 240, 40);

        jPanel1.add(jLayeredPane1);
        jLayeredPane1.setBounds(10, 10, 370, 570);

        menu.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        menu.setForeground(new java.awt.Color(159, 181, 193));
        menu.setText("Menu");
        menu.setBorder(null);
        menu.setBorderPainted(false);
        menu.setContentAreaFilled(false);
        menu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuActionPerformed(evt);
            }
        });
        jPanel1.add(menu);
        menu.setBounds(790, 280, 120, 50);

        start.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        start.setForeground(new java.awt.Color(159, 181, 193));
        start.setText("Start");
        start.setBorder(null);
        start.setBorderPainted(false);
        start.setContentAreaFilled(false);
        start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startActionPerformed(evt);
            }
        });
        jPanel1.add(start);
        start.setBounds(600, 280, 120, 50);

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_small.png"))); // NOI18N
        jPanel1.add(jLabel6);
        jLabel6.setBounds(790, 280, 130, 50);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_small_background.png"))); // NOI18N
        jPanel1.add(jLabel5);
        jLabel5.setBounds(790, 280, 130, 50);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_small.png"))); // NOI18N
        jPanel1.add(jLabel3);
        jLabel3.setBounds(600, 280, 130, 50);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/display_small_background.png"))); // NOI18N
        jPanel1.add(jLabel4);
        jLabel4.setBounds(600, 280, 130, 50);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/choseplayer.png"))); // NOI18N
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, -30, 1130, 580);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1130, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 551, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jSlider1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jSlider1MouseClicked
        JSlider Slider=(JSlider)evt.getSource();
        if(evt.getX()<43)
        {
            Slider.setValue(2);
        }
        else if(evt.getX()<86)
        {
            Slider.setValue(3);
        }
        else if(evt.getX()<129)
        {
            Slider.setValue(4);
        }
        else if(evt.getX()<172)
        {
            Slider.setValue(5);
        }
        else if(evt.getX()<215)
        {
            Slider.setValue(6);
        }
        else if(evt.getX()<258)
        {
            Slider.setValue(7);
        }
        else if(evt.getX()<301)
        {
            Slider.setValue(8);
        }

    }//GEN-LAST:event_jSlider1MouseClicked

    private void menuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuActionPerformed
        // TODO add your handling code here:
        Meniu menu = new Meniu();
        dispose();
        menu.setVisible(true);
    }//GEN-LAST:event_menuActionPerformed

    private void startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startActionPerformed
        // TODO add your handling code here:
        n = jSlider1.getValue();
        dev = jDev.isSelected();
        smoney = Integer.parseInt(jMoney.getText());
        pmoney = Integer.parseInt(jMoneyPass.getText());
        Interface interfata = new Interface();
        dispose();
        interfata.setVisible(true);
    }//GEN-LAST:event_startActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ChosePlayer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ChosePlayer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChosePlayer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChosePlayer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>


        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ChosePlayer().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox jDev;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JTextField jMoney;
    private javax.swing.JTextField jMoneyPass;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JButton menu;
    private javax.swing.JButton start;
    // End of variables declaration//GEN-END:variables
}
