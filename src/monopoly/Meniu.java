/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;


/**
 *
 * @author Kocsis
 */
public class Meniu extends javax.swing.JFrame {

    public void Cursor()
    {
        Toolkit tool = Toolkit.getDefaultToolkit();
        Image curs = tool.getImage("bin/images/cursor.png");
        Point point = new Point(0,0);
        Cursor cursor = tool.createCustomCursor(curs,point,"Cursor");
        setCursor(cursor);
    }
    
    public Meniu() 
    {
        initComponents();
        Cursor();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        Play1 = new javax.swing.JButton();
        Quit = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Monopoly Space Adventure");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setIconImages(null);

        jPanel1.setLayout(null);

        Play1.setBackground(new java.awt.Color(39, 39, 39));
        Play1.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Play1.setForeground(new java.awt.Color(153, 153, 153));
        Play1.setText("Play");
        Play1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        Play1.setBorderPainted(false);
        Play1.setContentAreaFilled(false);
        Play1.setDefaultCapable(false);
        Play1.setFocusPainted(false);
        Play1.setFocusable(false);
        Play1.setRequestFocusEnabled(false);
        Play1.setVerifyInputWhenFocusTarget(false);
        Play1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Play1ActionPerformed(evt);
            }
        });
        jLayeredPane1.add(Play1);
        Play1.setBounds(60, 50, 250, 50);

        Quit.setBackground(new java.awt.Color(39, 39, 39));
        Quit.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        Quit.setForeground(new java.awt.Color(153, 153, 153));
        Quit.setText("Quit");
        Quit.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        Quit.setBorderPainted(false);
        Quit.setContentAreaFilled(false);
        Quit.setDefaultCapable(false);
        Quit.setFocusPainted(false);
        Quit.setFocusable(false);
        Quit.setRequestFocusEnabled(false);
        Quit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                QuitActionPerformed(evt);
            }
        });
        jLayeredPane1.add(Quit);
        Quit.setBounds(60, 120, 250, 50);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/buton_on.png"))); // NOI18N
        jLayeredPane1.add(jLabel4);
        jLabel4.setBounds(60, 120, 250, 50);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/buton_on.png"))); // NOI18N
        jLayeredPane1.add(jLabel3);
        jLabel3.setBounds(60, 50, 250, 50);

        jPanel1.add(jLayeredPane1);
        jLayeredPane1.setBounds(400, 370, 370, 210);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagini/principal_menu.jpg"))); // NOI18N
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, 0, 1130, 590);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1130, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 584, Short.MAX_VALUE)
                .addGap(1, 1, 1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Play1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Play1ActionPerformed
        ChosePlayer chose = new ChosePlayer();
        dispose();
        chose.setVisible(true);
    }//GEN-LAST:event_Play1ActionPerformed

    private void QuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_QuitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_QuitActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Meniu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Meniu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Meniu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Meniu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Meniu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Play1;
    private javax.swing.JButton Quit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
