/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import static monopoly.Chance.value2pay;
import static monopoly.ChosePlayer.pmoney;
import static monopoly.Economy.ShowPlayerInfo;
import static monopoly.Interface.Build;
import static monopoly.Interface.Buy;
import static monopoly.Interface.Buys;
import static monopoly.Interface.End;
import static monopoly.Interface.Ok;
import static monopoly.Interface.Roll;
import static monopoly.Interface.RoundDisplay;
import static monopoly.Interface.zar1;
import static monopoly.Interface.zar2;
import static monopoly.Location.posx;
import static monopoly.Location.posy;
import static monopoly.Interface.Sell;
import static monopoly.Interface.info;
import static monopoly.Interface.infosell;
import static monopoly.Interface.jLabel_ok;
import static monopoly.Interface.jRent;
import static monopoly.Player.dead;
import static monopoly.Player.glow;
import static monopoly.Player.labels;



/**
 *
 * @author Leonescu
 */

public class Move 
{
    
    //<editor-fold defaultstate="collapsed" desc="Declaration">
    
    
    Timer timer;
    public static Interface inter = new Interface();
    Chance change = new Chance();
    int i =0;
    int loc;
    static boolean t=false;
    public static boolean ok;
    public static boolean buy;
    public static boolean build;
    public static boolean jrent;
    public static boolean buys;
    public static int dice;
    public static int kill;
    public static int win;
    public static int pion;
    public static int stay;
    public static int rolezar1;
    public static int rolezar2;
    public static int overstart;  //after start
    public static int untilstart;  //until start
    public static int locbefore; //save location before passing over start
    public static int[] location;
    // </editor-fold>
    
    
    
    public static void init()
    {
        value2pay=0;
        kill=0;
        win=0;
        pion=0;
        location = new int[]{0,0,0,0,0,0,0,0};
        overstart=0;
        untilstart=0;
        locbefore=0;
    }
    
    public void End() 
    {
        glow[pion].setVisible(false);
        info.setVisible(true);
        infosell.setText("");
        Roll.setEnabled(true);
        inter.jLabel_roll_on.setVisible(true);
        inter.jLabel_roll_off.setVisible(false);
        Sell.setVisible(false);
        End.setEnabled(false);
        inter.jLabel_end_on.setVisible(false);
        inter.jLabel_end_off.setVisible(true);
        Buy.setVisible(false);
        Build.setVisible(false);
        jLabel_ok.setVisible(false);
        
        //<editor-fold defaultstate="collapsed" desc="ChangePlayer"> 
        pion++;
        if(pion == ChosePlayer.n)
            pion = 0;
        // </editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="WinDisplay">
        Interface.info.setText("");
        
        if(kill == ChosePlayer.n-1){
            switch (Player.id[pion]) {
                case 1:
                    Interface.info.setText("<html>" +
                        "<center><p>" + "Announcement:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "Player1 Won!" + "</p></center>" +
                        "</html>");
                    break;
                case 2:
                    Interface.info.setText("<html>" +
                        "<center><p>" + "Announcement:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "Player2 Won!" + "</p></center>" +
                        "</html>");
                    break;
                case 3:
                    Interface.info.setText("<html>" +
                        "<center><p>" + "Announcement:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "Player3 Won!" + "</p></center>" +
                        "</html>");
                    break;
                case 4:
                    Interface.info.setText("<html>" +
                        "<center><p>" + "Announcement:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "Player4 Won!" + "</p></center>" +
                        "</html>");
                    
                    break;
                case 5:
                    Interface.info.setText("<html>" +
                        "<center><p>" + "Announcement:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "Player5 Won!" + "</p></center>" +
                        "</html>");
                    
                    break;
                case 6:
                    Interface.info.setText("<html>" +
                        "<center><p>" + "Announcement:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "Player6 Won!" + "</p></center>" +
                        "</html>");
                    break;
                case 7:
                    Interface.info.setText("<html>" +
                        "<center><p>" + "Announcement:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "Player7 Won!" + "</p></center>" +
                        "</html>");
                    break;
                case 8:
                    Interface.info.setText("<html>" +
                        "<center><p>" + "Announcement:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "Player8 Won!" + "</p></center>" +
                        "</html>");
                    break;
                default:
                    break;       
            }
            
            Roll.setEnabled(false);
            inter.jLabel_roll_off.setVisible(true);
            inter.jLabel_roll_on.setVisible(false);
            
        }
        // </editor-fold>

        //<editor-fold defaultstate="collapsed" desc="RoundDisplay">
        if(Player.dead[pion]==false)
        { 
            glow[pion].setVisible(true);
            RoundDisplay.setText("Player" + Integer.toString(pion + 1));
        }
        else
        {
            End();
        }
        // </editor-fold>
        
    }
    
    public void Moves()
    {
        
        int delay = 250; //milliseconds
        dice = rolezar1 + rolezar2;
        loc = location[pion];
        
        ActionListener taskPerformer = (ActionEvent evt) -> 
        {           
            i++;
            location[pion]+=1;
            
            if(location[pion]-1==posx.length)
            {
                Player.money[pion]+=pmoney;
                labels[pion].setBounds(posx[0]  + pion + 5, posy[0]  + pion - 5, 20, 20);
                loc=1;
                location[pion]=1;
            }
            else
            {
                labels[pion].setBounds(posx[location[pion]-1]  + pion + 5, posy[location[pion]-1]  + pion - 5, 20, 20);    
            }
            if(i==dice)
                {
                    timer.stop();
                    i=0;
                    End.setEnabled(true);
                    inter.jLabel_end_off.setVisible(false);
                    inter.jLabel_end_on.setVisible(true);
                    DisplayBuy();
                    DisplaySpacial();
                }
            
            ShowPlayerInfo(Player.id[pion]);
        };
        
        timer = new Timer(delay, taskPerformer);
        timer.setRepeats(true);
        timer.start();
        
        
 
    }
    
    public void RollAction(java.awt.event.ActionEvent evt) 
    {     
        
        value2pay=0;
        Dice dice = new Dice(); 
        Roll.setEnabled(false);
        inter.jLabel_roll_off.setVisible(true);
        inter.jLabel_roll_on.setVisible(false);
        
        
        //<editor-fold defaultstate="collapsed" desc="Roll">
        rolezar1 = dice.RollDice();
        rolezar2 = dice.RollDice();
        zar1.setText(Integer.toString(rolezar1));
        zar2.setText(Integer.toString(rolezar2));
        // </editor-fold>
        
        Moves();
        
        

    } 
    
    public void DisplaySpacial()
    {
        
        switch (location[pion]) {
            
            case 5:
                labels[pion].setBounds(posx[22]  + pion + 5, posy[22]  + pion - 5, 20, 20);
                location[pion] = 23;
                info.setText("<html>" + 
                    "<center><p>" + "Warning" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "You fell in a wormhole!" + "</p></center>" +
                    "</html>");
                break;
            case 23:
                labels[pion].setBounds(posx[4]  + pion + 5, posy[4]  + pion - 5, 20, 20);
                location[pion] = 5;
                info.setText("<html>" +
                    "<center><p>" + "Warning" + "</p></center>" +
                    "<center><p>" + "</p></center>" + 
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "You fell in a wormhole!" + "</p></center>" +
                    "</html>");
                break;
            case 14:
                labels[pion].setBounds(posx[31]  + pion + 5, posy[31]  + pion - 5, 20, 20);
                location[pion] = 32;
                info.setText("<html>" +
                    "<center><p>" + "Warning" + "</p></center>" +
                    "<center><p>" + "</p></center>" + 
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "You fell in a wormhole!" + "</p></center>" +
                    "</html>");
                break;
            case 32:
                labels[pion].setBounds(posx[13]  + pion + 5, posy[13]  + pion - 5, 20, 20);
                location[pion] = 14;
                info.setText("<html>" +
                    "<center><p>" + "Warning" + "</p></center>" +
                    "<center><p>" + "</p></center>" + 
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "You fell in a wormhole!" + "</p></center>" +
                    "</html>");
                break;
            case 18:
                Player.lifes[pion] -= 1;
                ShowPlayerInfo(Player.id[pion]);
                info.setText("<html>" +
                    "<center><p>" + "Supernova" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "</p></center>" + 
                    "<center><p>" + "Your ship was destroyed!" + "</p></center>" +
                    "</html>");
                End.setEnabled(false);
                inter.jLabel_end_off.setVisible(true);
                inter.jLabel_end_on.setVisible(false);
                Ok.setVisible(true);
                jLabel_ok.setVisible(true);
                break;
            case 16:
                info.setText("<html>" +
                    "<center><p>" + "Bar:" + "</p></center>" +
                    "<center><p>" + "</p></center>" +  
                    "<center><p>" + "You were in a barfight. You paid 1000$." + "</p></center>" +
                    "</html>");
                value2pay=1000;
                Ok.setVisible(true);
                jLabel_ok.setVisible(true);
                End.setEnabled(false);
                inter.jLabel_end_off.setVisible(true);
                inter.jLabel_end_on.setVisible(false);
                break;
            case 34:
                info.setText("<html>" +
                    "<center><p>" + "Bar:" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "You were in a barfight. You paid 1000$." + "</p></center>" +
                    "</html>");
                value2pay=1000;
                Ok.setVisible(true);
                jLabel_ok.setVisible(true);
                End.setEnabled(false);
                inter.jLabel_end_off.setVisible(true);
                inter.jLabel_end_on.setVisible(false);
                break;
            
                
            case 3:
                change.Draw();
                End.setEnabled(false);
                inter.jLabel_end_off.setVisible(true);
                inter.jLabel_end_on.setVisible(false);
                Ok.setVisible(true);
                jLabel_ok.setVisible(true);
                break;
            case 12:
                change.Draw();
                End.setEnabled(false);
                inter.jLabel_end_off.setVisible(true);
                inter.jLabel_end_on.setVisible(false);
                Ok.setVisible(true);
                jLabel_ok.setVisible(true);
                break;
            case 21:
                change.Draw();
                End.setEnabled(false);
                inter.jLabel_end_off.setVisible(true);
                inter.jLabel_end_on.setVisible(false);
                Ok.setVisible(true);
                jLabel_ok.setVisible(true);
                break;
            case 30:
                change.Draw();
                End.setEnabled(false);
                inter.jLabel_end_off.setVisible(true);
                inter.jLabel_end_on.setVisible(false);
                Ok.setVisible(true);
                jLabel_ok.setVisible(true);
                break;
            default:
                break;
            }

    }
    
    public void DisplayBuy()
    {
       
        //<editor-fold defaultstate="collapsed" desc="Canbuy">
        if(Location.sellable[location[pion]-1]== true)
        {
            if(Player.money[pion]>Location.value[location[pion]-1])
            {
                Buy.setVisible(true);
                jLabel_ok.setVisible(true);
                info.setText("<html>" +
                    "<center><p>" + Location.name[location[pion]-1] +"</p></center>" + 
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "Sector: " + Integer.toString(Location.value[location[pion]-1]) + "$" + "</p></center>" +
                    "</html>");
                
            }
            
            else
            {
                info.setText("<html>" +
                    "<center><p>" + Location.name[location[pion]-1] +"</p></center>" + 
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "You need " + Location.value[location[pion]-1] + " to buy sector." + "</p></center>" +
                    "</html>");
            }
        }
        // </editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="AddStation">
        
        if(Location.owner[location[pion]-1]==Player.id[pion])
        {
            if(Player.money[pion]>500)
            {
                info.setText("<html>" +
                "<center><p>" + Location.name[location[pion]-1] + "</p></center>" + 
                "<center><p>" + "</p></center>" + 
                "<center><p>" + "Income: " + Integer.toString(Location.rent[location[pion]-1]) + "$" + "</p></center>" + 
                "<center><p>" + "</p></center>" + 
                "<center><p>" + "Station: 500$" + "</p></center>" +
                "</html>");
                Build.setVisible(true);
                jLabel_ok.setVisible(true);
                
            }
            else
            {
                info.setText("<html>" +
                    "<center><p>" + Location.name[location[pion]-1] +"</p></center>" + 
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "Income: " + Integer.toString(Location.rent[location[pion]-1]) + "$" + "</p></center>" + 
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "You need 500$ buy a station." + "</p></center>" +
                    "</html>");
            }   
        }
        // </editor-fold> 
        
        //<editor-fold defaultstate="collapsed" desc="Rent">
        else if(Location.owner[location[pion]-1]!=0)
        {
            if(Location.rent[location[pion]-1]!=0)
            {
            info.setText("<html>" +
                "<center><p>" + Location.name[location[pion]-1] + "</p></center>" + 
                "<center><p>" + "</p></center>" +
                "<center><p>" + "</p></center>" +        
                "<center><p>" + "Pay "+
                Integer.toString(Location.rent[location[pion]-1]) + "$" + " passage fee." + "</p></center>" +
                "</html>");
                End.setEnabled(false);
                inter.jLabel_end_off.setVisible(true);
                inter.jLabel_end_on.setVisible(false);
                value2pay=Location.rent[location[pion]-1];
                jRent.setVisible(true);
                jLabel_ok.setVisible(true);
            }
            else
            {
            info.setText("<html>" +
                "<center><p>" + Location.name[location[pion]-1] + "</p></center>" + 
                "<center><p>" + "</p></center>" +
                "<center><p>" + "</p></center>" + 
                "<center><p>" + "There is nothing." + "</p></center>" +
                "</html>"); 
            End.setEnabled(false);
            inter.jLabel_end_off.setVisible(true);
            inter.jLabel_end_on.setVisible(false);
            Ok.setVisible(true);
            jLabel_ok.setVisible(true);

            }
            
        }
        // </editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="ShipBuy">
        switch (location[pion])
        {
            case 36:
                if (Player.money[pion]>5000)
                {
                    if(Player.lifes[pion]<3)
                    {
                    info.setText("<html>" + 
                        "<center><p>" + "Homeworld:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +  
                        "<center><p>" + "Welcome back!" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "Ship: 5000$" + "</p></center>" +
                        "</html>");
                    value2pay=5000;
                    Interface.Buys.setVisible(true);
                    jLabel_ok.setVisible(true);
                    }
                    else
                    {
                    info.setText("<html>" + 
                        "<center><p>" + "Homeworld:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +  
                        "<center><p>" + "Welcome back!" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "You can't have more ships." + "</p></center>" +
                        "</html>");    
                    }
                }
                else
                {
                    info.setText("<html>" + 
                        "<center><p>" + "Homeworld:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +  
                        "<center><p>" + "Welcome back!" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "You need 5000$ to buy a ship." + "</p></center>" +
                        "</html>");
                }
                break;
            case 9:
                if (Player.money[pion]>5000)
                {
                    if(Player.lifes[pion]<3)
                    {
                    info.setText("<html>" + 
                        "<center><p>" + "Hub:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +  
                        "<center><p>" + "Welcome to hub!" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "Ship: 5000$" + "</p></center>" +
                        "</html>");
                    Interface.Buys.setVisible(true);
                    jLabel_ok.setVisible(true);
                    }
                    else
                    {
                    info.setText("<html>" + 
                        "<center><p>" + "Hub:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +  
                        "<center><p>" + "Welcome to hub!" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "You can't have more ships." + "</p></center>" +
                        "</html>");    
                    }
                }
                else
                {
                    info.setText("<html>" + 
                        "<center><p>" + "Hub:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +  
                        "<center><p>" + "Welcome to hub!" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "You need 5000$ to buy a ship." + "</p></center>" +
                        "</html>");
                }
                break;
            case 27:
                if (Player.money[pion]>5000)
                {
                    if(Player.lifes[pion]<3)
                    {
                    info.setText("<html>" + 
                        "<center><p>" + "Hub:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +  
                        "<center><p>" + "Welcome to hub!" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "Ship: 5000$" + "</p></center>" +
                        "</html>");
                    Interface.Buys.setVisible(true);
                    jLabel_ok.setVisible(true);
                    }
                    else
                    {
                    info.setText("<html>" + 
                        "<center><p>" + "Hub:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +  
                        "<center><p>" + "Welcome to hub!" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "You can't have more ships." + "</p></center>" +
                        "</html>");    
                    }
                }
                else
                {
                    info.setText("<html>" + 
                        "<center><p>" + "Hub:" + "</p></center>" +
                        "<center><p>" + "</p></center>" +  
                        "<center><p>" + "Welcome to hub!" + "</p></center>" +
                        "<center><p>" + "</p></center>" +
                        "<center><p>" + "You need 5000$ to buy a ship." + "</p></center>" +
                        "</html>");
                }
                break;
        }
        // </editor-fold>
    
    }
    
    public static void Return()
    {
        
        Sell.setVisible(false);
        jLabel_ok.setVisible(false);
        
        //<editor-fold defaultstate="collapsed" desc="ReturnButtonState">
        if(ok)
        {
            Ok.setVisible(true);
            jLabel_ok.setVisible(true);
        }
        if(buys)
        {
            Buys.setVisible(true);
            jLabel_ok.setVisible(true);
        }
        if(jrent)
        {
            jRent.setVisible(true);
            jLabel_ok.setVisible(true);
        }
        // </editor-fold>
        
        
        info.setVisible(true);
        infosell.setText("");
        Interface.p=0;
        DeadCheck();
    }
    
    public static void DisplaySell(int p)
    {
        if (Location.owner[p-1]==Player.id[pion])
        {
            info.setVisible(false);
            
            //<editor-fold defaultstate="collapsed" desc="SaveButtonState">
            jrent=jRent.isVisible();
            build=Build.isVisible();
            ok=Ok.isVisible();
            buy=Buy.isVisible();
            buys=Buys.isVisible();
            // </editor-fold>
            
            Sell.setVisible(true);
            Ok.setVisible(false);
            Buy.setVisible(false);
            Buys.setVisible(false);
            Build.setVisible(false);
            jRent.setVisible(false);
            jLabel_ok.setVisible(true);
            infosell.setText("<html>" +
                "<center><p>" + Location.name[p-1] +"</p></center>" + 
                "<center><p>" + "</p></center>" +
                "<center><p>" + "Stations: " + Location.stations[p-1] +
                "<center><p>" + "</p></center>" +
                "<center><p>" + "Sector: " + Integer.toString(Location.value[p-1]/2+Location.stations[p-1]*250/2) + "$" + "</p></center>" +
                "</html>");  
        }
    }
    
    public static void DeadCheck()
    {
        
        End.setEnabled(true);
        inter.jLabel_end_off.setVisible(false);
        inter.jLabel_end_on.setVisible(true);
        
        
        if(Player.money[pion] <= 0)
        {
            //<editor-fold defaultstate="collapsed" desc="Checklifes">
            if(Player.lifes[pion]>1)
            {
                info.setText("<html>" + 
                    "<center><p>" + "Announcement:" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "Out of funds!" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "Ship: 2500$" + "</p></center>" +
                    "</html>");
                    value2pay=-2500;
                    t=true;
                End.setEnabled(false);
                inter.jLabel_end_off.setVisible(true);
                inter.jLabel_end_on.setVisible(false);
                Ok.setVisible(true);
                jLabel_ok.setVisible(true);
            }
            // </editor-fold>
            
            //<editor-fold defaultstate="collapsed" desc="Checksectors">
            else if(Player.sectors[pion]!=0)
            {
                info.setText("<html>" + 
                    "<center><p>" + "Announcement:" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "Out of funds!" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "Sell Sectors." + "</p></center>" +
                    "</html>");
                t=true;
                End.setEnabled(false);
                inter.jLabel_end_off.setVisible(true);
                inter.jLabel_end_on.setVisible(false);
                Ok.setVisible(false);
                jLabel_ok.setVisible(false);
            }
            // </editor-fold>
            
            else
            {
                dead();
                info.setText("<html>" + 
                    "<center><p>" + "Announcement:" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "Out of funds!" + "</p></center>" +
                    "</html>");
            }
        }
        else if(Player.lifes[pion] <= 0)
        {
            dead();
            info.setText("<html>" + 
                    "<center><p>" + "Announcement:" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "Out of ships!" + "</p></center>" +
                    "</html>");
        }
        else if(Player.dead[pion]==true)
        {
            dead();
            info.setText("<html>" + 
                "<center><p>" + "Announcement:" + "</p></center>" +
                "<center><p>" + "</p></center>" +
                "<center><p>" + "You have died!" + "</p></center>" +
                "</html>");
        }
        else
        {
            if (t==true)
            {
                info.setText("<html>" + 
                    "<center><p>" + "Announcement:" + "</p></center>" +
                    "<center><p>" + "</p></center>" +
                    "<center><p>" + "You have funds again!" + "</p></center>" +
                    "</html>");
                t=false;
                Ok.setVisible(false);
                jLabel_ok.setVisible(false);
            }
        }
    }
    
}
