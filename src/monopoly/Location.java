/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly;

import javax.swing.JLabel;
import static monopoly.Interface.*;
import static monopoly.Move.location;
import static monopoly.Move.pion;


/**
 *
 * @author Leonescu
 */

public class Location 
{
    
    //<editor-fold defaultstate="collapsed" desc="Declaration">
    public static String[] name;
    public static int[] value;
    public static int[] rent;
    public static int[] id;  
    public static int[] owner;
    public static int[] stations;
    public static JLabel[] stationsl;
    public static JLabel[] locplayer;
    public static boolean[] sellable;
    public static int[] posx;
    public static int[] posy;
    // </editor-fold>
    
    public static void init()
    {
        name = new String[]{"Mercury","Venus","","Moon","","Mars","Phobos","Deimos","","Jupiter","Io","","Europa","","Saturn","","Titan","","Uranus","Oberon","","Ariel","","Neptune","Triton","Pluto","","Skore","Acloria","","Sudrea","","Tathor","","Thiea",""};
        value = new int[]{500,600,0,700,0,1000,1400,1800,0,2000,2400,0,2800,0,3400,0,3800,0,4000,4400,0,4800,0,5000,5400,5800,0,6000,6400,0,6800,0,7400,0,7800};
        rent=new int[36];
        id=new int[36];
        owner=new int[36];
        stations=new int[36];
        stationsl = new JLabel[]{sd1,sd2,sd3,sd4,sd5,sd6,sd7,sd8,sd9,sd10,sd11,sd12,sd13,sd14,sd15,sd16,sd17,sd18,sd19,sd20,sd21,sd22,sd23,sd24,sd25,sd26,sd27,sd28,sd29,sd30,sd31,sd32,sd33,sd34,sd35,sd36};
        locplayer = new JLabel[]{loc1,loc2,loc3,loc4,loc5,loc6,loc7,loc8,loc9,loc10,loc11,loc12,loc13,loc14,loc15,loc16,loc17,loc18,loc19,loc20,loc21,loc22,loc23,loc24,loc25,loc26,loc27,loc28,loc29,loc30,loc31,loc32,loc33,loc34,loc35,loc36};
        sellable = new boolean[36];
        posx = new int[]{85, 135, 185, 235, 285, 335, 385, 435, 505, 505, 505, 505, 505, 505, 505, 505, 505, 505, 435, 385, 335, 285, 235, 185, 135,  85,  25,  25,  25,  25,  25,  25,  25,  25, 25, 25};
        posy = new int[]{40,  40,  40,  40,  40,  40,  40,  40,  40, 90, 140, 190, 240, 290, 340, 390, 440, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 440, 390, 340, 290, 240, 190, 140, 90, 40};
        
        for(int i=0;i<value.length;i++){
            stations[i]=0;
            id[i]=i+1;
            rent[i]=value[i]/10*stations[i];
            if(value[i]==0) 
                sellable[i]=false;
            else 
                sellable[i]=true;
            owner[i]=0;
            stationsl[i].setBackground(new java.awt.Color(151,151,151));
            
        }
        
    }
    
    public static void update_color(int id)
    {
        switch (id) {
            
            case 0:
                stationsl[location[pion] - 1].setBackground(new java.awt.Color(255, 0, 1));
                break;
            case 1:
                stationsl[location[pion] - 1].setBackground(new java.awt.Color(28, 6, 237));
                break;
            case 2:
                stationsl[location[pion] - 1].setBackground(new java.awt.Color(50, 234, 7));
                break;
            case 3:
                stationsl[location[pion] - 1].setBackground(new java.awt.Color(216, 234, 7));
                break;
            case 4:
                stationsl[location[pion] - 1].setBackground(new java.awt.Color(7, 204, 237));
                break;
            case 5:
                stationsl[location[pion] - 1].setBackground(new java.awt.Color(235, 6, 168));
                break;
            case 6:
                stationsl[location[pion] - 1].setBackground(new java.awt.Color(235, 114, 8));
                break;
            case 7:
                stationsl[location[pion] - 1].setBackground(new java.awt.Color(6, 98, 3));
                break;
            default:
                break;
        }
    }
    
}
