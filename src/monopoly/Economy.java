/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly;


import static monopoly.Interface.Buy;
import static monopoly.Interface.info;
import static monopoly.Interface.jLabel_ok;
import static monopoly.Location.rent;
import static monopoly.Location.stations;
import static monopoly.Location.update_color;
import static monopoly.Location.value;
import static monopoly.Move.Return;
import static monopoly.Move.location;
import static monopoly.Move.pion;

/**
 *
 * @author Leonescu
 */
public class Economy 
{
    static Move mov = new Move();
    
    public static void ShowPlayerInfo(int id)
    {
        switch (id) {
            
            case 1:
                Interface.Money1.setText(Integer.toString(Player.money[id-1]) + "$");
            switch (Player.lifes[id-1]) {
            case 3:
                Interface.rac_p1_1.setVisible(true);
                Interface.rac_p1_2.setVisible(true);
                Interface.rac_p1_3.setVisible(true);
                break;
            case 2:
                Interface.rac_p1_1.setVisible(true);
                Interface.rac_p1_2.setVisible(true);
                Interface.rac_p1_3.setVisible(false);
                break;
            case 1:
                Interface.rac_p1_1.setVisible(true);
                Interface.rac_p1_2.setVisible(false);
                Interface.rac_p1_3.setVisible(false);
                break;
            case 0:
                Interface.rac_p1_1.setVisible(false);
                Interface.rac_p1_2.setVisible(false);
                Interface.rac_p1_3.setVisible(false);
                break;
            default:
                break;
            }
                break;
            case 2:
                Interface.Money2.setText(Integer.toString(Player.money[id-1]) + "$");
                switch (Player.lifes[id-1]) {
            case 3:
                Interface.rac_p2_1.setVisible(true);
                Interface.rac_p2_2.setVisible(true);
                Interface.rac_p2_3.setVisible(true);
                break;
            case 2:
                Interface.rac_p2_1.setVisible(true);
                Interface.rac_p2_2.setVisible(true);
                Interface.rac_p2_3.setVisible(false);
                break;
            case 1:
                Interface.rac_p2_1.setVisible(true);
                Interface.rac_p2_2.setVisible(false);
                Interface.rac_p2_3.setVisible(false);
                break;
            case 0:
                Interface.rac_p2_1.setVisible(false);
                Interface.rac_p2_2.setVisible(false);
                Interface.rac_p2_3.setVisible(false);
                break;
            default:
                break;
            }
                break;
            case 3:
                Interface.Money3.setText(Integer.toString(Player.money[id-1]) + "$");
                switch (Player.lifes[id-1]) {
            case 3:
                Interface.rac_p3_1.setVisible(true);
                Interface.rac_p3_2.setVisible(true);
                Interface.rac_p3_3.setVisible(true);
                break;
            case 2:
                Interface.rac_p3_1.setVisible(true);
                Interface.rac_p3_2.setVisible(true);
                Interface.rac_p3_3.setVisible(false);
                break;
            case 1:
                Interface.rac_p3_1.setVisible(true);
                Interface.rac_p3_2.setVisible(false);
                Interface.rac_p3_3.setVisible(false);
                break;
            case 0:
                Interface.rac_p3_1.setVisible(false);
                Interface.rac_p3_2.setVisible(false);
                Interface.rac_p3_3.setVisible(false);
                break;
            default:
                break;
            }
                break;
            case 4:
                Interface.Money4.setText(Integer.toString(Player.money[id-1]) + "$");
                switch (Player.lifes[id-1]) {
            case 3:
                Interface.rac_p4_1.setVisible(true);
                Interface.rac_p4_2.setVisible(true);
                Interface.rac_p4_3.setVisible(true);
                break;
            case 2:
                Interface.rac_p4_1.setVisible(true);
                Interface.rac_p4_2.setVisible(true);
                Interface.rac_p4_3.setVisible(false);
                break;
            case 1:
                Interface.rac_p4_1.setVisible(true);
                Interface.rac_p4_2.setVisible(false);
                Interface.rac_p4_3.setVisible(false);
                break;
            case 0:
                Interface.rac_p4_1.setVisible(false);
                Interface.rac_p4_2.setVisible(false);
                Interface.rac_p4_3.setVisible(false);
                break;
            default:
                break;
            }
                break;
            case 5:
                Interface.Money5.setText(Integer.toString(Player.money[id-1]) + "$");
                switch (Player.lifes[id-1]) {
            case 3:
                Interface.rac_p5_1.setVisible(true);
                Interface.rac_p5_2.setVisible(true);
                Interface.rac_p5_3.setVisible(true);
                break;
            case 2:
                Interface.rac_p5_1.setVisible(true);
                Interface.rac_p5_2.setVisible(true);
                Interface.rac_p5_3.setVisible(false);
                break;
            case 1:
                Interface.rac_p5_1.setVisible(true);
                Interface.rac_p5_2.setVisible(false);
                Interface.rac_p5_3.setVisible(false);
                break;
            case 0:
                Interface.rac_p5_1.setVisible(false);
                Interface.rac_p5_2.setVisible(false);
                Interface.rac_p5_3.setVisible(false);
                break;
            default:
                break;
            }
                break;
            case 6:
                Interface.Money6.setText(Integer.toString(Player.money[id-1]) + "$");
                switch (Player.lifes[id-1]) {
            case 3:
                Interface.rac_p6_1.setVisible(true);
                Interface.rac_p6_2.setVisible(true);
                Interface.rac_p6_3.setVisible(true);
                break;
            case 2:
                Interface.rac_p6_1.setVisible(true);
                Interface.rac_p6_2.setVisible(true);
                Interface.rac_p6_3.setVisible(false);
                break;
            case 1:
                Interface.rac_p6_1.setVisible(true);
                Interface.rac_p6_2.setVisible(false);
                Interface.rac_p6_3.setVisible(false);
                break;
            case 0:
                Interface.rac_p6_1.setVisible(false);
                Interface.rac_p6_2.setVisible(false);
                Interface.rac_p6_3.setVisible(false);
                break;
            default:
                break;
            }
                break;
            case 7:
                Interface.Money7.setText(Integer.toString(Player.money[id-1]) + "$");
                switch (Player.lifes[id-1]) {
            case 3:
                Interface.rac_p7_1.setVisible(true);
                Interface.rac_p7_2.setVisible(true);
                Interface.rac_p7_3.setVisible(true);
                break;
            case 2:
                Interface.rac_p7_1.setVisible(true);
                Interface.rac_p7_2.setVisible(true);
                Interface.rac_p7_3.setVisible(false);
                break;
            case 1:
                Interface.rac_p7_1.setVisible(true);
                Interface.rac_p7_2.setVisible(false);
                Interface.rac_p7_3.setVisible(false);
                break;
            case 0:
                Interface.rac_p7_1.setVisible(false);
                Interface.rac_p7_2.setVisible(false);
                Interface.rac_p7_3.setVisible(false);
                break;
            default:
                break;
            }
                break;
            case 8:
                Interface.Money8.setText(Integer.toString(Player.money[id-1]) + "$");
                switch (Player.lifes[id-1]) {
            case 3:
                Interface.rac_p8_1.setVisible(true);
                Interface.rac_p8_2.setVisible(true);
                Interface.rac_p8_3.setVisible(true);
                break;
            case 2:
                Interface.rac_p8_1.setVisible(true);
                Interface.rac_p8_2.setVisible(true);
                Interface.rac_p8_3.setVisible(false);
                break;
            case 1:
                Interface.rac_p8_1.setVisible(true);
                Interface.rac_p8_2.setVisible(false);
                Interface.rac_p8_3.setVisible(false);
                break;
            case 0:
                Interface.rac_p8_1.setVisible(false);
                Interface.rac_p8_2.setVisible(false);
                Interface.rac_p8_3.setVisible(false);
                break;
            default:
                break;
            }
                break;
            default:
                break;
        }
    }

    public static void Pay(int sum)
    {
        Player.money[pion]-=sum;
        ShowPlayerInfo(Player.id[pion]);
    }
    
    public static void Buy(int sum)
    {
        Player.sectors[pion]+=1;
        Pay(sum);
        ShowPlayerInfo(Location.owner[location[pion]-1]);
        Location.owner[location[pion]-1]=Player.id[pion];
        Location.sellable[location[pion]-1]=false;
        Buy.setVisible(false);
        jLabel_ok.setVisible(false);
        
        update_color(pion);
        
    }
    
    public static void Build()
    {
        Pay(500);
        Location.stations[location[pion]-1]+=1;
        rent[location[pion]-1]=value[location[pion]-1]/10*stations[location[pion]-1];
        Location.stationsl[location[pion]-1].setText(Integer.toString(Location.stations[location[pion]-1]));
        
        info.setText("<html>" + 
                "<center><p>" + Location.name[location[pion]-1] + "</p></center>" + 
                "<center><p>" + "</p></center>" +
                "<center><p>" + "Income: " + Integer.toString(Location.rent[location[pion]-1]) + "$" + "</p></center>" +
                "<center><p>" + "</p></center>" +
                "</html>");
    }
    
    public static void Rent(int sum)
    {
        Pay(sum);
        Player.money[Location.owner[location[pion]-1]-1]+=sum;
        ShowPlayerInfo(Location.owner[location[pion]-1]);
        mov.DeadCheck();
    }
    
    public static void Sell(int p)
    {
        Player.sectors[pion]-=1;
        Pay(-Location.value[p-1]/2+Location.stations[p-1]*250/2);
        Location.stationsl[p-1].setBackground(new java.awt.Color(151,151,151));
        Location.owner[p-1]=0;
        Location.stations[p-1]=0;
        Location.rent[p-1]=0;
        Location.sellable[p-1]=true;
        Location.stationsl[p-1].setText(" ");

        Return();
        
    }
 
}

